# Rasteroid

Rasteroid, [Asteroids](https://en.wikipedia.org/wiki/Asteroids_(video_game)) clone of the Atari arcade game (1979), written in Rust, used as Codeberg first project. The project is based on SDL2 external crate. Note that this code is written by a Rust beginner, coming from Java/C/C++ background. 

![Screenshot](images/rasteroid.png "Rasteroid")