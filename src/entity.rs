use crate::atlas::{Atlas, Shape};
use crate::atlas::Shape::{BigAsteroid, Point, Ship, SmallAsteroid};
use crate::constants::{BIG_ASTEROID_BOUNDING_RADIUS, BIG_ASTEROID_MASS, BIG_ASTEROID_SCORE_VALUE, PLAYER_SHIP_BOUNDING_RADIUS, SHIP_MASS, SMALL_ASTEROID_BOUNDING_RADIUS, SMALL_ASTEROID_MASS, SMALL_ASTEROID_SCORE_VALUE};
use crate::Vector2D;

#[derive(Clone)]
pub struct Entity {
    pub pos: Vector2D<f32>,
    // Vector position.
    pub speed: Vector2D<f32>,
    // Speed vector : m.s-1.
    pub accel: Vector2D<f32>,// Acceleration vector : m.s-2.
    pub f: Vector2D<f32>,
    // Force applied : newton.
    pub omega: f32,
    // Angular speed :degree.s-1 !
    pub theta: f32,
    // Angular position.
    pub scale_x: f32,
    // X scale
    pub scale_y: f32,
    // Y scale
    pub mass: f32,
    // mass kg.
    pub force_applied: bool,
    // Force/Motor on or off ?
    pub persistent: bool,
    // Does object respawn when it leaves screen ?
    pub reload_delay: f32,
    pub score_value: u32,
    pub bounding_circle_radius: f32,
    // Bounding box (circle) for collision detection.
    pub vertexes: &'static [(f32, f32)],
    //pub vertexes: &'a VectorDrawable.
}


impl Entity {
    /**
     * Build the default Entity.
     */
    pub fn new(shape: Shape, x: f32, y: f32) -> Entity {

        Entity {
            pos: Vector2D::new(x, y),
            speed: Default::default(),
            accel: Default::default(),
            f: Default::default(),
            omega: 0.0,
            theta: 0.0,
            scale_x: 1.0,
            scale_y: 1.0,
            mass: 0.0,
            force_applied: false,
            persistent: false,
            reload_delay: 0f32,
            score_value: 0,
            bounding_circle_radius: 0.0,
            vertexes: Atlas::get_vertexes(shape),
        }
    }

    /**
     * Build a new player ship at [0;0]
     */
    pub fn new_ship() -> Entity
    {
        Entity {
            pos: Vector2D::new(0f32, 0f32),
            speed: Default::default(),
            accel: Default::default(),
            f: Default::default(),
            omega: 0.0,
            theta: 0.0,
            scale_x: 1.0,
            scale_y: 1.0,
            mass: SHIP_MASS,
            force_applied: false,
            persistent: true,
            reload_delay: 0f32,
            score_value: 0,
            bounding_circle_radius: PLAYER_SHIP_BOUNDING_RADIUS,
            vertexes: Atlas::get_vertexes(Ship),
        }
    }

    pub fn new_big_asteroid(x: f32, y: f32) -> Entity
    {
        Entity {
            pos: Vector2D::new(x, y),
            speed: Default::default(),
            accel: Default::default(),
            f: Default::default(),
            omega: 0.0,
            theta: 0.0,
            scale_x: 1.0,
            scale_y: 1.0,
            mass: BIG_ASTEROID_MASS,
            force_applied: false,
            persistent: true,
            reload_delay: 0f32,
            score_value: BIG_ASTEROID_SCORE_VALUE,
            bounding_circle_radius: BIG_ASTEROID_BOUNDING_RADIUS,
            vertexes: Atlas::get_vertexes(BigAsteroid),
        }
    }

    pub fn new_small_asteroid(x: f32, y: f32) -> Entity
    {
        Entity {
            pos: Vector2D::new(x, y),
            speed: Default::default(),
            accel: Default::default(),
            f: Default::default(),
            omega: 0.0,
            theta: 0.0,
            scale_x: 1.0,
            scale_y: 1.0,
            mass: SMALL_ASTEROID_MASS,
            force_applied: false,
            persistent: true,
            reload_delay: 0f32,
            score_value: SMALL_ASTEROID_SCORE_VALUE,
            bounding_circle_radius: SMALL_ASTEROID_BOUNDING_RADIUS,
            vertexes: Atlas::get_vertexes(SmallAsteroid),
        }
    }

    pub fn new_missile(x: f32, y: f32) -> Entity
    {
        Entity {
            pos: Vector2D::new(x, y),
            speed: Default::default(),
            accel: Default::default(),
            f: Default::default(),
            omega: 0.0,
            theta: 0.0,
            scale_x: 1.0,
            scale_y: 1.0,
            mass: 0f32,
            force_applied: false,
            persistent: false,
            reload_delay: 0f32,
            score_value: 0,
            bounding_circle_radius: 0f32,
            vertexes: Atlas::get_vertexes(Point),
        }
    }
}
