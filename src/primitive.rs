/**
 * primitive.rs : This code is used to manipulate and convert Entities into Vec<Point> to
 * be used with SDL.
 *
 * Date :  28/09/2022 14h04
 * Author : Bruno Vedder
 */

pub mod primitive {
    use crate::atlas::Atlas;
    use crate::entity::Entity;
    use crate::{Matrix2D, Vector2D};
    use sdl2::rect::Point;


    /**
     * Received a Vector of PIXELS Entities that need to be transformed before rendering.
     * w : is the world matrix that setup screen as classical orthogonal system.
     * The returned is done in the ret vector.
     * Note that Matrix multiplication with : rotation and scale matrix are skipped due to the
     * nature of pixel entity.
     */
    pub fn to_point_primitive(entities: &Vec<Entity>, w: &Matrix2D<f32>, ret: &mut Vec<Point>)
    {
        ret.clear();

        for e in entities {
            // Transform entity in its own referential. No scaling or rotation for pixels.
            let t: Matrix2D<f32> = Matrix2D::new_translation(e.pos.x, e.pos.y);

            // Now convert do SDL (top-left 0,0) referential.
            let all: Matrix2D<f32> = *w * t;

            let mut v: Vector2D<f32> = Vector2D::from_tuple(&e.vertexes[0]);
            v = all * v;
            ret.push(Point::new(v.x as i32, v.y as i32));
        }
    }


    /**
     * Received a Vector of Entities that need to be transformed before rendering.
     * w : is the world matrix that setup screen as classical orthogonal system.
     * The returned is done in the ret vector.
     */
    #[allow(dead_code)]
    pub fn to_segment_primitive(entities: &Vec<Entity>, w: &Matrix2D<f32>, ret: &mut Vec<Point>)
    {
        ret.clear();

        for e in entities {
            // Transform entity in its own referential.
            let t: Matrix2D<f32> = Matrix2D::new_translation(e.pos.x, e.pos.y);
            let r: Matrix2D<f32> = Matrix2D::<f32>::new_rotation(e.theta);
            let s: Matrix2D<f32> = Matrix2D::new_scale(e.scale_x, e.scale_y);

            // Now convert do SDL (top-left 0,0) referential.
            let all: Matrix2D<f32> = *w * t * r * s;

            for i in 0..(e.vertexes.len()) {
                let mut v: Vector2D<f32> = Vector2D::from_tuple(&e.vertexes[i]);
                v = all * v;
                ret.push(Point::new(v.x as i32, v.y as i32));
            }
        }
    }


    /**
     * Received a Vector of Entities that need to be transformed before rendering.
     * w : is the world matrix that setup screen as classical orthogonal system.
     * The returned is done in the ret vector.
     */
    pub fn to_poly_lines_primitive(entities: &Vec<Entity>, w: &Matrix2D<f32>, ret: &mut Vec<Point>, rind: &mut Vec<usize>)
    {
        ret.clear();
        rind.clear();
        let mut cumul: usize = 0;

        for e in entities {
            // Transform entity in its own referential.
            let t: Matrix2D<f32> = Matrix2D::new_translation(e.pos.x, e.pos.y);
            let r: Matrix2D<f32> = Matrix2D::<f32>::new_rotation(e.theta);
            let s: Matrix2D<f32> = Matrix2D::new_scale(e.scale_x, e.scale_y);

            // Now convert do SDL (top-left 0,0) referential.
            let all: Matrix2D<f32> = *w * t * r * s;
            let l = e.vertexes.len();
            cumul += l;
            for i in 0..(l) {
                let mut v: Vector2D<f32> = Vector2D::from_tuple(&e.vertexes[i]);
                v = all * v;
                ret.push(Point::new(v.x as i32, v.y as i32));
            }

            rind.push(cumul);
        }
    }


    pub fn to_single_poly_line(e: &Entity, w: &Matrix2D<f32>, ret: &mut Vec<Point>)
    {
        ret.clear();

        // Transform entity in its own referential.
        let t: Matrix2D<f32> = Matrix2D::new_translation(e.pos.x, e.pos.y);
        let r: Matrix2D<f32> = Matrix2D::<f32>::new_rotation(e.theta);
        let s: Matrix2D<f32> = Matrix2D::new_scale(e.scale_x, e.scale_y);

        // Now convert do SDL (top-left 0,0) referential.
        let all: Matrix2D<f32> = *w * t * r * s;
        let l = e.vertexes.len();

        for i in 0..(l) {
            let mut v: Vector2D<f32> = Vector2D::from_tuple(&e.vertexes[i]);
            v = all * v;
            ret.push(Point::new(v.x as i32, v.y as i32));
        }
    }


    /**
     * Build a Vec<Point> made of screen CENTERED Segment primitives corresponding to the given
     * String reference.
     */
    pub fn get_centered_string_primitive(y: f32, spacing: f32, sx: f32, sy: f32, msg: &str, w: &Matrix2D<f32>) -> Vec<Point>
    {
        let x = -(get_string_width(spacing, sx, msg) / 2f32);
        get_string_primitive(x, y, spacing, sx, sy, msg, w)
    }


    /**
     * Build a Vec<Point> made of Segment primitives corresponding to the given String reference.
     */
    pub fn get_string_primitive(x: f32, y: f32, spacing: f32, sx: f32, sy: f32, msg: &str, w: &Matrix2D<f32>) -> Vec<Point>
    {
        let mut cx = x;
        let s: Matrix2D<f32> = Matrix2D::new_scale(sx, sy);
        let mut p: Vec<Point> = Vec::with_capacity(msg.len() * 8); // Average 8 point per letter.

        // For all letters...
        for l in msg.bytes() {
            if l != 32
            // ignore space char
            {
                let t: Matrix2D<f32> = Matrix2D::new_translation(cx, y);
                let a = *w * t * s;

                let v = Atlas::get_shape_by_u8(l);
                let shape = v.expect("Trying to use a non existing char from Atlas !");
                let vtx: &'static [(f32, f32)] = Atlas::get_vertexes(shape);

                // For all vertexes of the shape...
                for v in vtx {
                    let vt = Vector2D::<f32>::from_tuple(v);
                    let vtp = a * vt;

                    let pt: Point = Point::new(vtp.x as i32, vtp.y as i32);
                    p.push(pt);
                }
            }
            cx = cx + (spacing * sx); // Move from given spacing, we don´t care letter's width !
        }

        p
    }


    /**
     * Build a Vec<Point> made of Segment primitives corresponding to the given String reference.
     * Computation is done via msg.len()-1 because spacing is used to compute NEXT letter position,
     * so it must not be taken in account in total length.
     */
    fn get_string_width(spacing: f32, sx: f32, msg: &str) -> f32 {
        (msg.len() - 1) as f32 * (spacing * sx)
    }
} // End of primitive Module.
