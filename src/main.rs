mod math;
mod atlas;
mod attract_mode;
mod constants;
mod entity;
mod physics;
mod primitive;
mod sdl_model;
mod sdl_vector_screen;
mod rng;
mod builder;
mod in_game;



extern crate sdl2;

use crate::attract_mode::AttractMode;
use crate::constants::{SCREEN_HEIGHT, SCREEN_WIDTH};
use crate::in_game::InGame;
use crate::math::matrix_2d::Matrix2D;
use crate::sdl_model::{InterModelMsg, SDLModel};
use crate::sdl_vector_screen::VectorScreen;
use crate::math::vector_2d::Vector2D;

fn main() -> Result<(), String> {
    let mut vec_screen: VectorScreen = VectorScreen::new("Rasteroid", SCREEN_WIDTH, SCREEN_HEIGHT);
    let mut model : Box<dyn SDLModel> = Box::new(AttractMode{});

    'main_loop: loop {

        let ret = model.run(&mut vec_screen);
        match ret
        {
            InterModelMsg::Exit => {
                break 'main_loop
            },
            InterModelMsg::StartGame => {
                model = Box::new(InGame{});
            },
            InterModelMsg::Error(msg) => {
                println!("An error occurs: {}", msg);
                break 'main_loop
            }
            InterModelMsg::GameFinished => {
                model = Box::new(AttractMode{});
            }
        }
    }
    Ok(())
}

