/**
 * Atlas : Structure holding arrays of VectorDrawable that contians vertexes as tuple of (f32,f32).
 * Theses data are static, so only a reference can be stored in entity insteand of n instances of
 * the same data.
 *
 * Date :  23/09/2022 8h30
 * Author : Bruno Vedder
 */
pub struct VectorDrawable {
    pub vertexes: &'static [(f32, f32)],
}


pub struct Atlas {}


impl Atlas {
    pub fn get_vertexes(s: Shape) -> &'static [(f32, f32)] {
        ATLAS[s as u8 as usize].vertexes
    }

    pub fn get_shape_by_u8(c: u8) -> Option<Shape> {
        match c {
            b'0' => Some(Shape::Zero),
            b'1' => Some(Shape::One),
            b'2' => Some(Shape::Two),
            b'3' => Some(Shape::Three),
            b'4' => Some(Shape::Four),
            b'5' => Some(Shape::Five),
            b'6' => Some(Shape::Six),
            b'7' => Some(Shape::Seven),
            b'8' => Some(Shape::Eight),
            b'9' => Some(Shape::Nine),
            b'A' => Some(Shape::A),
            b'B' => Some(Shape::B),
            b'C' => Some(Shape::C),
            b'D' => Some(Shape::D),
            b'E' => Some(Shape::E),
            b'F' => Some(Shape::F),
            b'G' => Some(Shape::G),
            b'H' => Some(Shape::H),
            b'I' => Some(Shape::I),
            b'J' => Some(Shape::J),
            b'K' => Some(Shape::K),
            b'L' => Some(Shape::L),
            b'M' => Some(Shape::M),
            b'N' => Some(Shape::N),
            b'O' => Some(Shape::O),
            b'P' => Some(Shape::P),
            b'Q' => Some(Shape::Q),
            b'R' => Some(Shape::R),
            b'S' => Some(Shape::S),
            b'T' => Some(Shape::T),
            b'U' => Some(Shape::U),
            b'V' => Some(Shape::V),
            b'W' => Some(Shape::W),
            b'X' => Some(Shape::X),
            b'Y' => Some(Shape::Y),
            b'Z' => Some(Shape::Z),
            b'.' => Some(Shape::Dot),
            b'%' => Some(Shape::Percent),
            _ => None, // Return an error !
        }
    }
}


#[derive(Copy, Clone, PartialEq)]
#[repr(u8)] // Maps enum on a u8 type.
pub enum Shape {
    Zero = 0,
    One,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    Dot,
    Percent,
    Point,
    BigAsteroid,
    SmallAsteroid,
    Ship,
    Mystship,
}


/**
 * Vector shapes database, only accessible via Atlas::get_vertexes(s : Shape).
 */
const ATLAS: [VectorDrawable; 43] = [
    // 10x15 Number 0 centered on origin.
    VectorDrawable {
        // ZERO
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, -7.5f32),
            (5f32, -7.5f32),
            (-5f32, -7.5f32),
            (-5f32, -7.5f32),
            (-5f32, 7.5),
        ],
    },
    // 10x15 Number 1 centered on origin.
    VectorDrawable {
        // ONE =
        vertexes: &[
            (0f32, 7.5f32),
            (0f32, -7.5f32),
            (0f32, 7.5f32),
            (-2f32, 5f32),
        ],
    },
    // 10x15 Number 2 centered on origin.
    VectorDrawable {
        //TWO =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, 0f32),
            (-5f32, 0f32),
            (5f32, 0f32),
            (-5f32, 0f32),
            (-5f32, -7.5f32),
            (-5f32, -7.5f32),
            (5f32, -7.5),
        ],
    },
    // 10x15 Number 3 centered on origin.
    VectorDrawable {
        // THREE =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (-5f32, 0f32),
            (5f32, 0f32),
            (-5f32, -7.5f32),
            (5f32, -7.5f32),
            (5f32, 7.5f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 Number 4 centered on origin.
    VectorDrawable {
        //FOUR =
        vertexes: &[
            (3f32, 7.5f32),
            (3f32, -7.5f32),
            (3f32, 7.5f32),
            (-5f32, 0f32),
            (-5f32, 0f32),
            (5f32, 0f32),
        ],
    },
    // 10x15 Number 5 centered on origin.
    VectorDrawable {
        // FIVE =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (-5f32, 7.5f32),
            (-5f32, 0f32),
            (-5f32, 0f32),
            (5f32, 0f32),
            (5f32, 0f32),
            (5f32, -7.5f32),
            (-5f32, -7.5f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 Number 6 centered on origin.
    VectorDrawable {
        // SIX =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (-5f32, 7.5f32),
            (-5f32, -7.5f32),
            (-5f32, 0f32),
            (5f32, 0f32),
            (5f32, 0f32),
            (5f32, -7.5f32),
            (-5f32, -7.5f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 Number 7 centered on origin.
    VectorDrawable {
        // SEVEN =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (-5f32, -7.5f32),
            (5f32, 7.5f32),
        ],
    },
    // 10x15 Number 8 centered on origin.
    VectorDrawable {
        // EIGHT =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, -7.5f32),
            (-5f32, 0f32),
            (5f32, 0f32),
            (5f32, -7.5f32),
            (-5f32, -7.5f32),
            (-5f32, -7.5f32),
            (-5f32, 7.5f32),
        ],
    },
    // 10x15 Number 9 centered on origin.
    VectorDrawable {
        //NINE =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, -7.5f32),
            (-5f32, 0f32),
            (5f32, 0f32),
            (5f32, -7.5f32),
            (-5f32, -7.5f32),
            (-5f32, 0f32),
            (-5f32, 7.5f32),
        ],
    },
    // 10x15 Number A centered on origin.
    VectorDrawable {
        //LET_A =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, -7.5f32),
            (-5f32, 0f32),
            (5f32, 0f32),
            (-5f32, -7.5f32),
            (-5f32, 7.5f32),
        ],
    },
    // 10x15 Number B centered on origin.
    VectorDrawable {
        // LET_B =
        vertexes: &[
            (-5f32, 7.5f32),
            (3f32, 7.5f32),
            (3f32, 7.5f32),
            (3f32, 0f32),
            (-5f32, 0f32),
            (5f32, 0f32),
            (5f32, 0f32),
            (5f32, -7.5f32),
            (-5f32, -7.5f32),
            (-5f32, 7.5f32),
            (-5f32, -7.5f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 Number C centered on origin.
    VectorDrawable {
        // LET_C =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (-5f32, 7.5f32),
            (-5f32, -7.5f32),
            (-5f32, -7.5f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 Number D centered on origin.
    VectorDrawable {
        // LET_D =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (-3f32, 7.5f32),
            (-3f32, -7.5f32),
            (5f32, 7.5f32),
            (5f32, -7.5f32),
            (-5f32, -7.5f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 NumberE centered on origin.
    VectorDrawable {
        // LET_E =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (-5f32, 7.5f32),
            (-5f32, -7.5f32),
            (-5f32, 0f32),
            (3f32, 0f32),
            (-5f32, -7.5f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 Number F centered on origin.
    VectorDrawable {
        //LET_F =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (-5f32, 7.5f32),
            (-5f32, -7.5f32),
            (-5f32, 0f32),
            (3f32, 0f32),
        ],
    },
    // 10x15 Number G centered on origin.
    VectorDrawable {
        // LET_G =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (-5f32, 7.5f32),
            (-5f32, -7.5f32),
            (-2f32, 0f32),
            (5f32, 0f32),
            (5f32, 0f32),
            (5f32, -7.5f32),
            (-5f32, -7.5f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 Number H centered on origin.
    VectorDrawable {
        // LET_H =
        vertexes: &[
            (5f32, 7.5f32),
            (5f32, -7.5f32),
            (5f32, 0f32),
            (-5f32, 0f32),
            (-5f32, -7.5f32),
            (-5f32, 7.5f32),
        ],
    },
    // 10x15 Number I centered on origin.
    VectorDrawable {
        // LET_I =
        vertexes: &[
            (2f32, 7.5f32),
            (-2f32, 7.5f32),
            (0f32, 7.5f32),
            (0f32, -7.5f32),
            (2f32, -7.5f32),
            (-2f32, -7.5f32),
        ],
    },
    // 10x15 Number J centered on origin.
    VectorDrawable {
        //LET_J =
        vertexes: &[
            (5f32, 7.5f32),
            (5f32, -5f32),
            (5f32, -5f32),
            (0f32, -7.5f32),
            (0f32, -7.5f32),
            (-2.5f32, -7.5f32),
            (-5f32, -5f32),
            (-2.5f32, -7.5f32),
        ],
    },
    // 10x15 Number K centered on origin.
    VectorDrawable {
        // LET_K =
        vertexes: &[
            (-5f32, 7.5f32),
            (-5f32, -7.5f32),
            (-5f32, 0f32),
            (3f32, 5f32),
            (-5f32, 0f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 Number L centered on origin.
    VectorDrawable {
        //LET_L =
        vertexes: &[
            (-5f32, 7.5f32),
            (-5f32, -7.5f32),
            (-5f32, -7.5f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 Number M centered on origin.
    VectorDrawable {
        //LET_M =
        vertexes: &[
            (-5f32, 7.5f32),
            (-5f32, -7.5f32),
            (-5f32, 7.5f32),
            (0f32, 0f32),
            (5f32, 7.5f32),
            (0f32, 0f32),
            (5f32, 7.5f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 Number N centered on origin.
    VectorDrawable {
        // LET_N =
        vertexes: &[
            (-5f32, 7.5f32),
            (-5f32, -7.5f32),
            (-5f32, 7.5f32),
            (5f32, -7.5f32),
            (5f32, 7.5f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 Number O centered on origin.
    VectorDrawable {
        // LET_O =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, -7.5f32),
            (5f32, -7.5f32),
            (-5f32, -7.5f32),
            (-5f32, -7.5f32),
            (-5f32, 7.5f32),
        ],
    },
    // 10x15 Number P centered on origin.
    VectorDrawable {
        // LET_P =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, 0f32),
            (-5f32, 0f32),
            (5f32, 0f32),
            (-5f32, -7.5f32),
            (-5f32, 7.5f32),
        ],
    },
    // 10x15 Number Q centered on origin.
    VectorDrawable {
        // LET_Q =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, -7.5f32),
            (5f32, -7.5f32),
            (-5f32, -7.5f32),
            (-5f32, -7.5f32),
            (-5f32, 7.5f32),
            (6f32, -8.5f32),
            (3f32, -6f32),
        ],
    },
    // 10x15 Number R centered on origin.
    VectorDrawable {
        // LET_R =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, 0f32),
            (-5f32, 0f32),
            (5f32, 0f32),
            (-5f32, 0f32),
            (5f32, -7.5f32),
            (-5f32, -7.5f32),
            (-5f32, 7.5f32),
        ],
    },
    // 10x15 Number S centered on origin.
    VectorDrawable {
        // LET_S =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (-5f32, 7.5f32),
            (-5f32, 0f32),
            (-5f32, 0f32),
            (5f32, 0f32),
            (5f32, 0f32),
            (5f32, -7.5f32),
            (-5f32, -7.5f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 Number T centered on origin.
    VectorDrawable {
        // LET_T =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (0f32, 7.5f32),
            (0f32, -7.5f32),
        ],
    },
    // 10x15 Number U centered on origin.
    VectorDrawable {
        // LET_U =
        vertexes: &[
            (-5f32, 7.5f32),
            (-5f32, -7.5f32),
            (5f32, 7.5f32),
            (5f32, -7.5f32),
            (-5f32, -7.5f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 Number V centered on origin.
    VectorDrawable {
        // LET_V =
        vertexes: &[
            (-5f32, 7.5f32),
            (0f32, -7.5f32),
            (5f32, 7.5f32),
            (0f32, -7.5f32),
        ],
    },
    // 10x15 Number W centered on origin.
    VectorDrawable {
        //LET_W =
        vertexes: &[
            (-5f32, 7.5f32),
            (-5f32, -7.5f32),
            (-5f32, -7.5f32),
            (0f32, 0f32),
            (5f32, -7.5f32),
            (0f32, 0f32),
            (5f32, 7.5f32),
            (5f32, -7.5f32),
        ],
    },
    // 10x15 Number X centered on origin.
    VectorDrawable {
        //LET_X =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, -7.5f32),
            (5f32, 7.5f32),
            (-5f32, -7.5f32),
        ],
    },
    // 10x15 Number Y centered on origin.
    VectorDrawable {
        // LET_Y =
        vertexes: &[
            (-5f32, 7.5f32),
            (0f32, -0f32),
            (5f32, 7.5f32),
            (0f32, 0f32),
            (0f32, 0f32),
            (0f32, -7.5f32),
        ],
    },
    // 10x15 Number Y centered on origin.
    VectorDrawable {
        // LET_Z =
        vertexes: &[
            (-5f32, 7.5f32),
            (5f32, 7.5f32),
            (5f32, 7.5f32),
            (-5f32, -7.5f32),
            (-5f32, -7.5f32),
            (5f32, -7.5f32),
        ],
    },
    VectorDrawable {
        // LET DOT 10x15 Number . centered on origin.
        vertexes: &[
            (0f32, -5f32), (0f32, -5f32),
        ],
    },
    VectorDrawable {
        // LET % 10x15 Number . centered on origin.
        vertexes: &[
            (-5f32, -7.5f32),
            (5f32, 7f32),
            (-5f32, 7f32), (-5f32, 7f32),
            (5f32, -7f32), (5f32, -7f32),
        ],
    },
    VectorDrawable {
        // SINGLE DOT
        vertexes: &[(0.0f32, 0.0f32)],
    },
    VectorDrawable {
        // BIG ASTEROID
        vertexes: &[
            (20.0f32, 0.0f32),
            (14.0f32, 14.0f32),
            (5.0f32, 5.0f32),
            (0.0f32, 20.0f32),
            (-14.0f32, 14.0f32),
            (-20.0f32, 0.0f32),
            (-14.0f32, -14.0f32),
            (0.0f32, -20.0f32),
            (14.0f32, -14.0f32),
            (20.0f32, 0.0f32),
        ],
    },
    VectorDrawable {
        // SMALL ASTEROID
        vertexes: &[
            (0f32, 10f32),
            (5f32, 10f32),
            (5f32, 5f32),
            (10f32, 0f32),
            (5f32, -5f32),
            (0f32, -10f32),
            (-10f32, -5f32),
            (-10f32, 5f32),
            (-7f32, 5f32),
            (-7f32, 7f32),
            (0f32, 10f32),
        ],
    },


    // Meteor (medium)

    VectorDrawable {
        // PLAYER SHIP
        vertexes: &[
            (0.0f32, 10.0f32),
            (10.0f32, -10.0f32),
            (0.0f32, -5.0f32),
            (-10.0f32, -10.0f32),
            (0.0f32, 10.0f32),
        ],
    },
    VectorDrawable {
        // Mystery ship
        vertexes: &[
            (5.0f32, 7.5f32),
            (5.0f32, 10f32),
            (-5.0f32, 10f32),
            (-5.0f32, 7.5f32),
            (-10.0f32, 7.5f32),
            (-15.0f32, 0f32),
            (-10.0f32, -7.5f32),
            (10.0f32, -7.50f32),
            (15.0f32, 0.0f32),
            (-15.0f32, 0.0f32),
            (15.0f32, 0.0f32),
            (10f32, 7.5f32),
            (-5.0f32, 7.5f32),
        ],
    },
];
