/**
 * Trait use on basic types. Implementations should return 0, the additive identity element.
 */
pub trait Zero {
    fn zero() -> Self;
}

impl Zero for f64 {
    fn zero() -> Self {
        0.0_f64
    }
}

impl Zero for f32 {
    fn zero() -> Self {
        0.0_f32
    }
}

impl Zero for i128 {
    fn zero() -> Self {
        0_i128
    }
}

impl Zero for i64 {
    fn zero() -> Self {
        0_i64
    }
}

impl Zero for i32 {
    fn zero() -> Self {
        0_i32
    }
}

impl Zero for i16 {
    fn zero() -> Self {
        0_i16
    }
}

impl Zero for i8 {
    fn zero() -> Self {
        0_i8
    }
}

impl Zero for u128 {
    fn zero() -> Self {
        0_u128
    }
}

impl Zero for u64 {
    fn zero() -> Self {
        0_u64
    }
}

impl Zero for u32 {
    fn zero() -> Self {
        0_u32
    }
}

impl Zero for u16 {
    fn zero() -> Self {
        0_u16
    }
}

impl Zero for u8 {
    fn zero() -> Self {
        0_u8
    }
}
