#![allow(dead_code)]

/**
 * Generic Vector2D class.
 * Vector2D<i32>, Vector2D<f64> etc...
 *
 * This class handles 2D basic vector math.
 * These vectors are used with matrix2D with homogeneous coordinates.
 * In homogeneous coordinate, w component is 0 or 1. 1 means the vector
 * represents a position and should be translated. 0 means direction Vector
 * which is senseless to translate.
 *
 * All integer arithmetics overflow is unhandled (user should handle it).
 * Vector2D<T>.norm() and normalize() are only provided for f32 and f64 types since sqrt()
 * is only available for f32 and f64.
 */
use crate::math::one::One;
use crate::math::zero::Zero;
use std::fmt;
use std::fmt::Formatter;
use std::ops::*;

#[derive(Debug, Copy, Clone)]
pub struct Vector2D<T> {
    pub x: T,
    pub y: T,
    pub w: T, // 0 or 1. 0 = Non translatable.
}

impl<T> fmt::Display for Vector2D<T>
where
    T: fmt::Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {}, w:{})", self.x, self.y, self.w)
    }
}

impl<T: PartialEq> PartialEq for Vector2D<T> {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

impl<T: Eq> Eq for Vector2D<T> {}

impl<T> Default for Vector2D<T>
where
    T: Zero + One,
{
    // Create new vector with Zero x, y component and 1 for w.
    fn default() -> Self {
        Vector2D {
            x: Zero::zero(),
            y: Zero::zero(),
            w: T::one(),
        }
    }
}

impl<T> Vector2D<T>
where
    T: One + Copy,
{
    // Create Vector from values.
    pub fn new(x1: T, y1: T) -> Self {
        Vector2D {
            x: x1,
            y: y1,
            w: T::one(),
        }
    }

    // Create Vector from tuple of type T.
    pub fn from_tuple(c: &(T, T)) -> Self {
        Vector2D {
            x: c.0,
            y: c.1,
            w: T::one(),
        }
    }
}

/**
 * Implementation of Vector2D<T> + Vector2D<T> operation. Allow infix notation
 * v3 = v2 + v1
 * Note : w is kept unchanged.
 */
impl<T> Add<Vector2D<T>> for Vector2D<T>
where
    T: Add<Output = T>,
{
    type Output = Vector2D<T>;
    fn add(self, other: Vector2D<T>) -> Self::Output {
        Vector2D {
            x: self.x + other.x,
            y: self.y + other.y,
            w: self.w,
        }
    }
}

/**
 * Implementation of Vector2D<T> - Vector2D<T> operation. Allow infix notation
 * v3 = v2 - v1
 * Note : w is kept unchanged.
 */
impl<T> Sub<Vector2D<T>> for Vector2D<T>
where
    T: Sub<Output = T>,
{
    type Output = Vector2D<T>;
    fn sub(self, other: Vector2D<T>) -> Self::Output {
        Vector2D {
            x: self.x - other.x,
            y: self.y - other.y,
            w: self.w,
        }
    }
}

/**
 * Implementation of Vector2D<T> * T operation. Allow infix notation
 * v3 = v2 * t
 * Note: t * v2 is not implemented. It should be done type by type:
 * https://users.rust-lang.org/t/operator-overloading-and-generics/77485/4
 * Note : w is kept unchanged.
 */
impl<T> Mul<T> for Vector2D<T>
where
    T: Copy + Mul<Output = T>,
{
    type Output = Vector2D<T>;
    fn mul(self, scale: T) -> Self::Output {
        Vector2D {
            x: self.x * scale,
            y: self.y * scale,
            w: self.w,
        }
    }
}

/**
 * Implementation of Vector2D<T> / T operation. Allow infix notation
 * v3 = v2 / t
 * Note: t / v2 is not implemented. It should be done type by type:
 * https://users.rust-lang.org/t/operator-overloading-and-generics/77485/4
 * Note : w is kept unchanged.
 */
impl<T> Div<T> for Vector2D<T>
where
    T: Copy + Div<Output = T>,
{
    type Output = Vector2D<T>;
    fn div(self, scale: T) -> Self::Output {
        Vector2D {
            x: self.x / scale,
            y: self.y / scale,
            w: self.w,
        }
    }
}

/**
 * Compute dot/scalar product.
 * Note: As we are using homogeneous coordinates, w is not considered as vector dimensional
 * component and is ignored from scalar() calculation.
 */
impl<T> Vector2D<T>
where
    T: Copy + Mul<Output = T> + Add<Output = T>,
{
    fn scalar(self: &Self, other: Vector2D<T>) -> T {
        (self.x * other.x) + (self.y * other.y)
    }
}

/**
 * Norm is only implemented for f64 or f32 types unless implementation is provided.
 * Note: As we are using homogeneous coordinates, w is not considered as vector dimensional
 * component and is ignored from norm() calculation.
 */
impl Vector2D<f64> {
    // Compute euclidean norm (length) of self vector.
    pub fn norm(self: &Self) -> f64 {
        let tmp: f64 = (self.x * self.x) + (self.y * self.y);
        tmp.sqrt()
    }

    // Return the very same vector scaled so its norm is 1.0
    pub fn normalize(self: &Self) -> Self {
        let len: f64 = self.norm();
        *self / len
    }
}

/**
 * From doc:
 * One should avoid implementing Into and implement From instead. Implementing From automatically
 * provides one with an implementation of Into thanks to the blanket implementation in the standard
 * library.
 *
 * Implements From trait, to build Vector<f64> from Vector<f32>.
 */
impl From<Vector2D<f32>> for Vector2D<f64> {
    fn from(f: Vector2D<f32>) -> Self {
        Vector2D {
            x: f.x as f64,
            y: f.y as f64,
            w: f.w as f64,
        }
    }
}

/**
 * From doc:
 * One should avoid implementing Into and implement From instead. Implementing From automatically
 * provides one with an implementation of Into thanks to the blanket implementation in the standard
 * library.
 *
 * Implements From trait, to build Vector<f32> from Vector<f64>.
 */
impl From<Vector2D<f64>> for Vector2D<f32> {
    fn from(f: Vector2D<f64>) -> Self {
        Vector2D {
            x: f.x as f32,
            y: f.y as f32,
            w: f.w as f32,
        }
    }
}

/**
 * Norm is only implemented for f64 or f32 types unless implementation is provided.
 * Note: As we are using homogeneous coordinates, w is not considered as vector dimensional
 * component and is ignored from norm() calculation.
 */
impl Vector2D<f32> {
    // Compute euclidean norm (length) of self vector.
    pub fn norm(self: &Self) -> f32 {
        let tmp: f32 = (self.x * self.x) + (self.y * self.y);
        tmp.sqrt()
    }

    // Return the very same vector scaled so its norm is 1.0
    pub fn normalize(self: &Self) -> Self {
        let len: f32 = self.norm();
        *self / len
    }
}

#[cfg(test)]
mod tests {
    use crate::math::vector_2d::Vector2D;

    #[test]
    fn test_constructors() {
        let _v1: Vector2D<f32> = Vector2D::default();
        let _v2: Vector2D<f32> = Vector2D::new(5.0, 30.0);
        let _v3: Vector2D<i8> = Vector2D { x: 5, y: 8, w: 1 };
    }

    #[test]
    fn test_equalities() {
        let v1: Vector2D<i8> = Vector2D { x: 5, y: 8, w: 1 };
        let v2: Vector2D<i8> = Vector2D { x: 5, y: 8, w: 1 };
        let v3: Vector2D<i8> = Vector2D { x: 5, y: 7, w: 1 };
        assert_eq!(v1, v2);
        assert_eq!(v2, v1);
        assert_ne!(v3, v1);
        assert_ne!(v1, v3);
    }

    #[test]
    fn test_sub() {
        let v1: Vector2D<i32> = Vector2D::new(486, 31);
        let v2: Vector2D<i32> = Vector2D::new(490, 587);
        let v3: Vector2D<i32> = v1 - v2;

        assert_eq!(v3, Vector2D::new(-4_i32, -556_i32));
    }

    #[test]
    fn test_add() {
        let v1: Vector2D<i32> = Vector2D::new(486, 31);
        let v2: Vector2D<i32> = Vector2D::new(490, 587);
        let v3: Vector2D<i32> = v1 + v2;

        assert_eq!(v3, Vector2D::new(976_i32, 618_i32));
    }

    #[test]
    fn test_mul() {
        let v1: Vector2D<i32> = Vector2D::new(486, 31);
        let v3 = v1 * 2;

        assert_eq!(v3, Vector2D::new(972_i32, 62_i32));
    }

    #[test]
    fn test_div() {
        let v1: Vector2D<i32> = Vector2D::new(486, 38);
        let v3 = v1 / 2;

        assert_eq!(v3, Vector2D::new(243_i32, 19_i32));
    }

    #[test]
    fn test_norm() {
        let v1: Vector2D<f64> = Vector2D::new(6., 9.);
        let n1 = v1.norm();
        assert!(n1 > 10.816);
        assert!(n1 < 10.817);

        let v1: Vector2D<f32> = Vector2D::new(6., 9.);
        let n1 = v1.norm();
        assert!(n1 > 10.816);
        assert!(n1 < 10.817);
    }

    #[test]
    fn test_normalize() {
        let mut v1: Vector2D<f64> = Vector2D::new(6., 6.);
        v1 = v1.normalize();
        let n1 = v1.norm();
        assert_eq!(n1, 1.);
    }

    #[test]
    fn test_scalar() {
        let v1: Vector2D<f32> = Vector2D::new(2., 2.);
        let v2: Vector2D<f32> = Vector2D::new(-2., 2.);
        let n1 = v1.scalar(v2);
        let n2 = v2.scalar(v1);

        assert_eq!(n1, 0.);
        assert_eq!(n2, 0.);

        let v1: Vector2D<i32> = Vector2D::new(-2, 3);
        let v2: Vector2D<i32> = Vector2D::new(4, 5);
        let n1 = v1.scalar(v2);
        assert_eq!(n1, 7);
    }

    #[test]
    fn test_from() {
        let v1: Vector2D<f32> = Vector2D::new(3.15, 8.61);
        let v2: Vector2D<f64> = Vector2D::from(v1);
        let v3: Vector2D<f32> = Vector2D::from(v2);
        assert_eq!(v1, v3);
    }

    #[test]
    fn test_into() {
        let v1: Vector2D<f32> = Vector2D::new(3.15, 8.61);
        let v2: Vector2D<f64> = v1.into();
        let v3: Vector2D<f32> = v2.into();
        assert_eq!(v1, v3);
    }
}
