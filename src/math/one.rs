/**
 * Trait use on basic types. Implementations should return 1, the multiplicative identity element.
 * Mainly used for Vector2D w<T> and Matrix2D, for implementing Default trait, and returning
 * Vector2D<T> with w = 1_u8 or 1.0f32
 * Or Identity matrix with diagonal 1 of any type <T>.
 */
pub trait One {
    fn one() -> Self;
}

impl One for f64 {
    fn one() -> Self {
        1.0_f64
    }
}

impl One for f32 {
    fn one() -> Self {
        1.0_f32
    }
}

impl One for i128 {
    fn one() -> Self {
        1_i128
    }
}

impl One for i64 {
    fn one() -> Self {
        1_i64
    }
}

impl One for i32 {
    fn one() -> Self {
        1_i32
    }
}

impl One for i16 {
    fn one() -> Self {
        1_i16
    }
}

impl One for i8 {
    fn one() -> Self {
        1_i8
    }
}

impl One for u128 {
    fn one() -> Self {
        1_u128
    }
}

impl One for u64 {
    fn one() -> Self {
        1_u64
    }
}

impl One for u32 {
    fn one() -> Self {
        1_u32
    }
}

impl One for u16 {
    fn one() -> Self {
        1_u16
    }
}

impl One for u8 {
    fn one() -> Self {
        1_u8
    }
}
