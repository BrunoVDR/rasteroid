#![allow(dead_code)]

/**
 * Generic Matrix2D struct.
 * Implements matrix operations using with also 2D vectors in homogeneous coordinates. This
 * mean 3x3 Matrix for two dimensional space.
 *
 * Matrix representation in double[9] array:
 *
 * | 0 | 1 | 2 | First line matrix indexes in the [9] array. -------------------
 * | 3 | 4 | 5 | Second line matrix indexes in the [9] array. ------------------
 * | 6 | 7 | 8 | Third line matrix indexes in the [9] array. -------------------
 *
 * Rotation matrix is only provided for f32 and f64 type because of cos() sin() availability.
 *
 */
use crate::math::minus_one::MinusOne;
use crate::math::one::One;
use crate::math::zero::Zero;
use crate::math::vector_2d::Vector2D;

use std::f64::consts::PI;
use std::fmt;
use std::fmt::Formatter;
use std::ops::{Add, Mul};

#[derive(Debug, Copy, Clone)]
pub struct Matrix2D<T> {
    pub m: [T; 9],
}

impl<T> fmt::Display for Matrix2D<T>
where
    T: fmt::Display,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "[({} {} {}) ({} {} {}) ({} {} {})]",
            self.m[0],
            self.m[1],
            self.m[2],
            self.m[3],
            self.m[4],
            self.m[5],
            self.m[6],
            self.m[7],
            self.m[8]
        )
    }
}

/**
 *  Implementation of default, for Matrix2D. Return the identity matrix.
 */
impl<T> Default for Matrix2D<T>
where
    T: Default + One,
{
    fn default() -> Self {
        Matrix2D {
            m: [
                One::one(),
                Default::default(),
                Default::default(),
                Default::default(),
                One::one(),
                Default::default(),
                Default::default(),
                Default::default(),
                One::one(),
            ],
        }
    }
}

impl<T: PartialEq> PartialEq for Matrix2D<T> {
    fn eq(&self, other: &Self) -> bool {
        for i in 0..9 {
            if self.m[i] != other.m[i] {
                return false;
            }
        }
        true
    }
}

impl<T: Eq> Eq for Matrix2D<T> {}

impl<T> Matrix2D<T>
where
    T: Clone,
{
    pub fn new(other: &Matrix2D<T>) -> Matrix2D<T> {
        other.clone()
    }
}

impl<T> Matrix2D<T>
where
    T: Zero + One,
{
    /**
     * Return a translation matrix to be used with Vector2D.
     *
     * | 1 | 0 | tx |
     * | 0 | 1 | ty |
     * | 0 | 0 | 1  |
     *
     * @param tx : x axis translation.
     * @param ty : y axis translation.
     */
    pub fn new_translation(tx: T, ty: T) -> Matrix2D<T> {
        Matrix2D {
            m: [
                One::one(),
                Zero::zero(),
                tx,
                Zero::zero(),
                One::one(),
                ty,
                Zero::zero(),
                Zero::zero(),
                One::one(),
            ],
        }
    }

    /**
     * Return a scaling matrix.
     *
     * | sx| 0 | 0 |
     * | 0 | sy| 0 |
     * | 0 | 0 | 1 |
     *
     * @param sx : x axis scale factor.
     * @param sy : y axis scale factor.
     */
    pub fn new_scale(sx: T, sy: T) -> Matrix2D<T> {
        Matrix2D {
            m: [
                sx,
                Zero::zero(),
                Zero::zero(),
                Zero::zero(),
                sy,
                Zero::zero(),
                Zero::zero(),
                Zero::zero(),
                One::one(),
            ],
        }
    }
}

impl Matrix2D<f32> {
    /**
     * Return plane rotation matrix.
     *
     * | cos t | -sin t | 0 |
     * | sin t |  cos t | 0 |
     * |   0   |    0   | 1 |
     *
     * @param theta : Anti clockwise trigonometric angle, in degree.
     */
    pub fn new_rotation(t: f32) -> Matrix2D<f32> {
        let theta: f32 = t * std::f32::consts::PI / 180.0f32;
        Matrix2D {
            m: [
                theta.cos(),
                -theta.sin(),
                Zero::zero(),
                theta.sin(),
                theta.cos(),
                Zero::zero(),
                Zero::zero(),
                Zero::zero(),
                One::one(),
            ],
        }
    }
}

impl Matrix2D<f64> {
    /**
     * Return plane rotation matrix.
     *
     * | cos t | -sin t | 0 |
     * | sin t |  cos t | 0 |
     * |   0   |    0   | 1 |
     *
     * @param theta : Anti clockwise trigonometric angle, in degree.
     */
    pub fn new_rotation(t: f64) -> Matrix2D<f64> {
        let theta: f64 = t * PI / 180.0f64;
        Matrix2D {
            m: [
                theta.cos(),
                -theta.sin(),
                Zero::zero(),
                theta.sin(),
                theta.cos(),
                Zero::zero(),
                Zero::zero(),
                Zero::zero(),
                One::one(),
            ],
        }
    }
}

impl<T> Matrix2D<T>
where
    T: Zero + One + MinusOne,
{
    /**
     * Returns a Matrix that, when multiplied with column coordinate, performs
     * a Symmetry around Y axis of the frame so only X coordinates are changed.
     *
     * | -1| 0 | 0 |
     * | 0 | 1 | 0 |
     * | 0 | 0 | 1 |
     */
    pub fn new_symmetry_y() -> Matrix2D<T> {
        Matrix2D {
            m: [
                MinusOne::minus_one(),
                Zero::zero(),
                Zero::zero(),
                Zero::zero(),
                One::one(),
                Zero::zero(),
                Zero::zero(),
                Zero::zero(),
                One::one(),
            ],
        }
    }

    /**
     * Returns a Matrix that, when multiplied with column coordinate, performs
     * a Symmetry around X axis of the frame so only Y coordinates are changed.
     *
     * | 1 | 0 | 0 |
     * | 0 |-1 | 0 |
     * | 0 | 0 | 1 |
     */
    pub fn new_symmetry_x() -> Matrix2D<T> {
        Matrix2D {
            m: [
                One::one(),
                Zero::zero(),
                Zero::zero(),
                Zero::zero(),
                MinusOne::minus_one(),
                Zero::zero(),
                Zero::zero(),
                Zero::zero(),
                One::one(),
            ],
        }
    }

    /**
     * Returns a Matrix that, when multiply with column coordinate, performs
     * a Symmetry from origin of the frame, so both X and Y are changed.
     *
     * |-1 | 0 | 0 |
     * | 0 |-1 | 0 |
     * | 0 | 0 | 1 |
     */
    pub fn new_symmetry_origin() -> Matrix2D<T> {
        Matrix2D {
            m: [
                MinusOne::minus_one(),
                Zero::zero(),
                Zero::zero(),
                Zero::zero(),
                MinusOne::minus_one(),
                Zero::zero(),
                Zero::zero(),
                Zero::zero(),
                One::one(),
            ],
        }
    }
}

/**
 * Implementation of Matrix2D<T> + Matrix2D<T> operation. Allow infix notation
 * v3 = v2 + v1
 */
impl<T> Add<Matrix2D<T>> for Matrix2D<T>
where
    T: Add<Output = T> + Copy,
{
    type Output = Matrix2D<T>;
    fn add(self, other: Matrix2D<T>) -> Self::Output {
        Matrix2D {
            m: [
                self.m[0] + other.m[0],
                self.m[1] + other.m[1],
                self.m[2] + other.m[2],
                self.m[3] + other.m[3],
                self.m[4] + other.m[4],
                self.m[5] + other.m[5],
                self.m[6] + other.m[6],
                self.m[7] + other.m[7],
                self.m[8] + other.m[8],
            ],
        }
    }
}

impl<T> Mul<Matrix2D<T>> for Matrix2D<T>
where
    T: Mul<Output = T> + Copy + Add<Output = T>,
{
    type Output = Matrix2D<T>;

    /**
     * Implement Matrix2D * Matrix2D -> Matrix2D operation.
     */
    fn mul(self, rhs: Matrix2D<T>) -> Self::Output {
        Matrix2D {
            m: [
                self.m[0] * rhs.m[0] + self.m[1] * rhs.m[3] + self.m[2] * rhs.m[6],
                self.m[0] * rhs.m[1] + self.m[1] * rhs.m[4] + self.m[2] * rhs.m[7],
                self.m[0] * rhs.m[2] + self.m[1] * rhs.m[5] + self.m[2] * rhs.m[8],
                self.m[3] * rhs.m[0] + self.m[4] * rhs.m[3] + self.m[5] * rhs.m[6],
                self.m[3] * rhs.m[1] + self.m[4] * rhs.m[4] + self.m[5] * rhs.m[7],
                self.m[3] * rhs.m[2] + self.m[4] * rhs.m[5] + self.m[5] * rhs.m[8],
                self.m[6] * rhs.m[0] + self.m[7] * rhs.m[3] + self.m[8] * rhs.m[6],
                self.m[6] * rhs.m[1] + self.m[7] * rhs.m[4] + self.m[8] * rhs.m[7],
                self.m[6] * rhs.m[2] + self.m[7] * rhs.m[5] + self.m[8] * rhs.m[8],
            ],
        }
    }
}

impl<T> Mul<T> for Matrix2D<T>
where
    T: Mul<Output = T> + Copy,
{
    type Output = Matrix2D<T>;

    /**
     * Implement Matrix2D * Scalar -> Matrix2D operation.
     */
    fn mul(self, rhs: T) -> Self::Output {
        Matrix2D {
            m: [
                self.m[0] * rhs,
                self.m[1] * rhs,
                self.m[2] * rhs,
                self.m[3] * rhs,
                self.m[4] * rhs,
                self.m[5] * rhs,
                self.m[6] * rhs,
                self.m[7] * rhs,
                self.m[8] * rhs,
            ],
        }
    }
}

impl<T> Mul<Vector2D<T>> for Matrix2D<T>
where
    T: Mul<Output = T> + Copy + Zero + One + Add<Output = T>,
{
    type Output = Vector2D<T>;

    /**
     * Implement Matrix2D * Vector2D -> Vector2D operation
     * (Matrix multiplication against COLUMN Vector2D).
     * This choice implies to use TRS matrix combination instead of common SRT.
     * S = Scaling
     * R = Rotation
     * T = Translation.
     *
     * | 0 | 1 | 2 |.....|0| ---------------------------------------------------
     * | 3 | 4 | 5 |.mul.|1| ---------------------------------------------------
     * | 6 | 7 | 8 |.....|2| ---------------------------------------------------
     *
     */
    fn mul(self, rhs: Vector2D<T>) -> Self::Output {
        Vector2D {
            x: self.m[0] * rhs.x + self.m[1] * rhs.y + self.m[2] * rhs.w,
            y: self.m[3] * rhs.x + self.m[4] * rhs.y + self.m[5] * rhs.w,
            w: self.m[6] * rhs.x + self.m[7] * rhs.y + self.m[8] * rhs.w,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{Matrix2D, Vector2D};
    use std::f64::consts::PI;

    #[test]
    fn test_equality() {
        let m1: Matrix2D<f32> = Matrix2D {
            m: [1., 2., 3., 1., 2., 3., 1., 2., 3.],
        };
        let m2: Matrix2D<f32> = Matrix2D {
            m: [1., 2., 3., 1., 2., 3., 1., 2., 3.],
        };
        let mi: Matrix2D<f32> = Matrix2D::default();

        assert_eq!(m1, m2);
        assert_ne!(m1, mi);
    }

    #[test]
    fn test_constructors() {
        let m1: Matrix2D<i8> = Matrix2D {
            m: [1, 2, 3, 1, 2, 3, 1, 2, 3],
        };
        let mut m2 = Matrix2D::new(&m1);
        assert_eq!(m1, m2);

        m2.m[4] = 0;
        assert_ne!(m1, m2);

        let m1: Matrix2D<i32> = Matrix2D {
            m: [1, 0, 4, 0, 1, 5, 0, 0, 1],
        };
        let m2: Matrix2D<i32> = Matrix2D::new_translation(4, 5);
        assert_eq!(m1, m2);

        let m1: Matrix2D<i32> = Matrix2D {
            m: [9, 0, 0, 0, 8, 0, 0, 0, 1],
        };
        let m2: Matrix2D<i32> = Matrix2D::new_scale(9, 8);
        assert_eq!(m1, m2);

        let m1: Matrix2D<i32> = Matrix2D {
            m: [-1, 0, 0, 0, 1, 0, 0, 0, 1],
        };
        let m2: Matrix2D<i32> = Matrix2D::new_symmetry_y();
        assert_eq!(m1, m2);

        let m1: Matrix2D<i32> = Matrix2D {
            m: [1, 0, 0, 0, -1, 0, 0, 0, 1],
        };
        let m2: Matrix2D<i32> = Matrix2D::new_symmetry_x();
        assert_eq!(m1, m2);

        let m1: Matrix2D<i32> = Matrix2D {
            m: [-1, 0, 0, 0, -1, 0, 0, 0, 1],
        };
        let m2: Matrix2D<i32> = Matrix2D::new_symmetry_origin();
        assert_eq!(m1, m2);

        let theta: f64 = 66.0f64 * PI / 180.0f64;
        let m1: Matrix2D<f64> = Matrix2D {
            m: [
                theta.cos(),
                -theta.sin(),
                0.,
                theta.sin(),
                theta.cos(),
                0.,
                0.,
                0.,
                1.,
            ],
        };
        let m2: Matrix2D<f64> = Matrix2D::<f64>::new_rotation(66.0f64);
        assert_eq!(m1, m2);

        let theta: f32 = 177.0f32 * std::f32::consts::PI / 180.0f32;
        let m1: Matrix2D<f32> = Matrix2D {
            m: [
                theta.cos(),
                -theta.sin(),
                0.,
                theta.sin(),
                theta.cos(),
                0.,
                0.,
                0.,
                1.,
            ],
        };
        let m2: Matrix2D<f32> = Matrix2D::<f32>::new_rotation(177.0f32);
        assert_eq!(m1, m2);
    }

    #[test]
    fn test_add() {
        let m0: Matrix2D<i32> = Matrix2D {
            m: [0, 0, 0, 0, 0, 0, 0, 0, 0],
        };
        let m1: Matrix2D<i32> = Matrix2D {
            m: [1, 2, 3, 4, 5, 6, 7, 8, 9],
        };
        let m2: Matrix2D<i32> = Matrix2D {
            m: [-1, -2, -3, -4, -5, -6, -7, -8, -9],
        };
        let m3 = m1 + m2;
        assert_eq!(m0, m3);

        let m0: Matrix2D<u32> = Matrix2D {
            m: [1, 2, 4, 8, 16, 32, 64, 128, 256],
        };
        let m1: Matrix2D<u32> = Matrix2D {
            m: [0, 1, 2, 4, 8, 16, 32, 64, 128],
        };
        let m2: Matrix2D<u32> = Matrix2D {
            m: [1, 1, 2, 4, 8, 16, 32, 64, 128],
        };
        let m3 = m1 + m2;
        assert_eq!(m0, m3);
    }

    #[test]
    fn test_mul_matmat() {
        let m0: Matrix2D<u32> = Matrix2D {
            m: [1, 2, 3, 4, 5, 6, 7, 8, 9],
        };
        let m1: Matrix2D<u32> = Matrix2D {
            m: [7, 6, 8, 0, 4, 5, 1, 3, 2],
        };
        let m2: Matrix2D<u32> = Matrix2D {
            m: [10, 23, 24, 34, 62, 69, 58, 101, 114],
        };
        let _m3 = m0 * m1;

        let m0: Matrix2D<u32> = Matrix2D::new(&m2); // m0 = m2
        let m1: Matrix2D<u32> = Matrix2D::default(); // Identity
        let m3 = m0 * m1;

        assert_eq!(m2, m3);
    }

    #[test]
    fn test_mul_scalmat() {
        let m0: Matrix2D<u32> = Matrix2D {
            m: [1, 2, 3, 4, 5, 6, 7, 8, 9],
        };
        let m1: Matrix2D<u32> = Matrix2D {
            m: [11, 22, 33, 44, 55, 66, 77, 88, 99],
        };
        let m2 = m0 * 11_u32;

        assert_eq!(m1, m2);
    }

    #[test]
    fn test_mul_vec() {
        let v0: Vector2D<i64> = Vector2D { x: 3, y: 7, w: 5 };
        let m0: Matrix2D<i64> = Matrix2D {
            m: [2, 3, -4, 11, 8, 7, 2, 5, 3],
        };
        let v1 = m0 * v0;
        let vr: Vector2D<i64> = Vector2D {
            x: 7,
            y: 124,
            w: 56,
        };
        assert_eq!(v1, vr);
    }
}
