/**
 * Trait use on basic types. Implementations should return -1, the multiplicative opposite element.
 * Mainly used for Vector2D w<T> and Matrix2D, for implementing Default trait, and Symmetry matrices.
 *
 * Note: Only signed and float types can implement this trait.
 */
pub trait MinusOne {
    fn minus_one() -> Self;
}

impl MinusOne for f64 {
    fn minus_one() -> Self {
        -1.0_f64
    }
}

impl MinusOne for f32 {
    fn minus_one() -> Self {
        -1.0_f32
    }
}

impl MinusOne for i128 {
    fn minus_one() -> Self {
        -1_i128
    }
}

impl MinusOne for i64 {
    fn minus_one() -> Self {
        -1_i64
    }
}

impl MinusOne for i32 {
    fn minus_one() -> Self {
        -1_i32
    }
}

impl MinusOne for i16 {
    fn minus_one() -> Self {
        -1_i16
    }
}

impl MinusOne for i8 {
    fn minus_one() -> Self {
        -1_i8
    }
}
