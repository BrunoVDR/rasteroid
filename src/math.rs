/**
 * math.rs : This code links various math modules in math sub-directory * be used with SDL.
 *
 * Date :  12/10/2022 09h34
 * Author : Bruno Vedder
 */

mod minus_one;
mod one;
mod zero;
pub mod vector_2d;
pub mod matrix_2d;





