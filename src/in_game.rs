use std::time;
use std::time::{Duration, Instant, SystemTime, UNIX_EPOCH};

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Point;

use crate::{InterModelMsg, Matrix2D, SDLModel, Vector2D, VectorScreen};
use crate::atlas::Shape::Ship;
use crate::builder::builder;
use crate::constants::*;
use crate::entity::Entity;
use crate::InterModelMsg::{Exit, GameFinished};
use crate::physics::physics;
use crate::primitive::primitive;
use crate::primitive::primitive::get_centered_string_primitive;
use crate::rng::rng::XorShift32;


pub(crate) struct InGame {}


impl SDLModel for InGame
{
    /**
     * This method is the InGame main loop.
     */
    fn run(self: &Self, vec_screen: &mut VectorScreen) -> InterModelMsg
    {
        let mut stage: u32 = 1;
        let mut lives: u32 = PLAYER_LIVES;

        let mut level_finished: bool;
        let mut game_finished: bool = false;
        let mut score = 0;

        let beam_color : Color = BEAM_COLOR;
        vec_screen.set_cross_beam_color(&CROSS_BEAM_COLOR);

        // Let's get w world matrix to access screen at classic orthogonal system.
        let w = vec_screen.get_world_matrix();

        let start = SystemTime::now();
        let since_the_epoch = start.duration_since(UNIX_EPOCH).expect("Time went backwards");
        let ms = since_the_epoch.as_millis();
        let to_xor = ms as u32;
        let seed = 0xD2B1_77FB ^ to_xor;

        let mut rng: XorShift32 = XorShift32::new(seed); // Should be mutable to alter state !

        let mut score_segments: Vec<Point>;

        // Allocate various entities vectors.
        let mut lives_poly_line_vec: Vec<Point> = Vec::with_capacity(48);
        let mut lives_poly_line_offsets: Vec<usize> = Vec::with_capacity(8);

        let mut poly_line_entities: Vec<Entity> = Vec::with_capacity(256);
        let mut poly_line_vec: Vec<Point> = Vec::with_capacity(1024);
        let mut poly_line_offsets: Vec<usize> = Vec::with_capacity(256);

        let mut point_entities: Vec<Entity> = Vec::with_capacity(1024);
        let mut point_vec: Vec<Point> = Vec::with_capacity(1024);

        let mut missiles_entities: Vec<Entity> = Vec::with_capacity(1024);
        let mut missiles_vec: Vec<Point> = Vec::with_capacity(1024);

        let mut fps_str: Vec<Point> = primitive::get_string_primitive(-310f32, 230f32, 14f32, 0.60f32, 0.75f32, "FPS", &w);
        let mut percent_str: Vec<Point> = primitive::get_string_primitive(-310f32, 215f32, 14f32, 0.60f32, 0.75f32, "PERCENT", &w);

        let mut player_vec_point: Vec<Point> = Vec::with_capacity(64);

        let frame_duration = time::Duration::from_nanos(16666667); // 16ms = 60Hz.
        let mut frames: u32 = 0;
        let mut cumul: Duration = Duration::new(0, 0);
        let mut fps_toggle = false;

        // Player' s control key state, true = pressed.
        let mut key_left: bool = false;
        let mut key_right: bool = false;
        let mut key_ctrl: bool = false;

        let mut events = vec_screen
            .get_sdl_context()
            .event_pump()
            .expect("Unable to pump SDL events:");

        loop {
            score_segments = get_centered_string_primitive(225f32, 14f32, 1f32, 1f32, &score.to_string(), &w);
            InGame::update_live(lives, &w, &mut lives_poly_line_vec, &mut lives_poly_line_offsets);
            builder::build_for_stage(stage, &mut poly_line_entities, &mut rng);
            let mut player_dead: bool = false;
            let mut animation_kept: f32 = KEEP_ANIMATION;
            let mut player: Entity = Entity::new_ship();
            let mut in_game_time: f32 = 0f32; // Time in game for the current life, seconds.
            let mut myst_ship: Option<f32> = InGame::rand_mystery_ship(&mut rng);
            level_finished = false;

            'ship_live: loop {
                let now = Instant::now();

                for event in events.poll_iter() {
                    match event {
                        Event::Quit { .. } => return Exit,
                        Event::KeyDown {
                            keycode: Some(keycode),
                            ..
                        } => {
                            if keycode == Keycode::Escape {
                                return Exit;
                            }

                            if keycode == Keycode::F {
                                fps_toggle = fps_toggle ^ true;
                            }

                            if keycode == Keycode::E {
                                let m = vec_screen.get_enhanced_rendering() ^true;
                                vec_screen.set_enhanced_rendering(m);
                            }

                            if keycode == Keycode::Up {
                                player.force_applied = true;
                            }
                            if keycode == Keycode::Left {
                                key_left = true;
                                key_right = false;
                            }
                            if keycode == Keycode::Right {
                                key_right = true;
                                key_left = false;
                            }
                            if keycode == Keycode::LCtrl {
                                key_ctrl = true;
                            }
                        }

                        Event::KeyUp {
                            keycode: Some(keycode),
                            ..
                        } => {
                            if keycode == Keycode::Up {
                                player.force_applied = false;
                            }
                            if keycode == Keycode::Left {
                                key_left = false;
                            }
                            if keycode == Keycode::Right {
                                key_right = false;
                            }
                            if keycode == Keycode::LCtrl {
                                key_ctrl = false;
                            }
                        }

                        _ => {}
                    }
                }


                // Clear Vector screen.
                vec_screen.clear(&CLEAR_COLOR);

                // Special treatment for player' ship.
                if !player_dead
                {
                    InGame::update_player_ship_state(&mut player, key_left, key_right, key_ctrl, &mut missiles_entities, 0.016, &mut rng);
                    physics::move_player_ship_entity(&mut player, &mut point_entities, 0.016, &mut rng);
                    primitive::to_single_poly_line(&mut player, &w, &mut player_vec_point);
                }

                physics::move_entities(&mut poly_line_entities, 0.016f32);
                physics::move_entities(&mut point_entities, 0.016f32);
                physics::move_entities(&mut missiles_entities, 0.016f32);

                if !player_dead
                {
                    let contact = InGame::test_player_collision(&player, &poly_line_entities);
                    if contact
                    {
                        builder::build_explosion(player.pos.x, player.pos.y, SHIP_EXPLOSION_PARTICLE_NBR, &mut point_entities, &mut rng);
                        player_dead = true;
                        lives = lives - 1;
                        if lives == 0
                        {
                            game_finished = true;
                        }
                    }
                }


                // Closure for use with Vec::retain().
                let closure = |m: &Entity| -> bool {
                    let r = InGame::test_missiles_asteroid_collision(m, &poly_line_entities);
                    if let Some(index) = r
                    {
                        let e: Entity = poly_line_entities.swap_remove(index);
                        if e.mass == BIG_ASTEROID_MASS
                        {
                            builder::build_asteroid_fragment(&e, BIG_ASTEROID_FRAGMENT_NBR, &mut poly_line_entities, &mut rng);
                        }
                        score += e.score_value;
                        score_segments = get_centered_string_primitive(225f32, 14f32, 1f32, 1f32, &score.to_string(), &w);
                        let sparks_nbr = if e.mass == BIG_ASTEROID_MASS { BIG_ASTEROID_EXPLOSION_PARTICLE_NBR } else { SMALL_ASTEROID_EXPLOSION_PARTICLE_NBR };
                        builder::build_explosion(e.pos.x, e.pos.y, sparks_nbr, &mut point_entities, &mut rng);

                        if poly_line_entities.len() == 0
                        {
                            level_finished = true;
                            stage = stage + 1;
                        }
                        return false;
                    }
                    true
                };

                // Only keep element on which closure returns true.
                missiles_entities.retain(closure);

                primitive::to_point_primitive(&missiles_entities, &w, &mut missiles_vec);
                primitive::to_point_primitive(&point_entities, &w, &mut point_vec);
                primitive::to_poly_lines_primitive(&poly_line_entities, &w, &mut poly_line_vec, &mut poly_line_offsets);

                // Start drawing
                vec_screen.set_beam_color(&beam_color);

                if fps_toggle
                {
                    vec_screen.draw_segments(&fps_str);
                    vec_screen.draw_segments(&percent_str);
                }

                // Render player ship
                if !player_dead
                {
                    vec_screen.draw_single_poly_lines(&player_vec_point);
                }
                vec_screen.draw_segments(&score_segments);
                vec_screen.draw_poly_lines(&poly_line_vec, &poly_line_offsets);
                vec_screen.draw_poly_lines(&lives_poly_line_vec, &lives_poly_line_offsets);
                vec_screen.draw_pixels(&missiles_vec);
                vec_screen.draw_pixels(&point_vec);

                // Refresh screen.
                vec_screen.present();

                let elapsed_time = now.elapsed();
                cumul = cumul + elapsed_time;
                frames += 1;

                if frames == FPS_TO_AVERAGE
                {
                    if fps_toggle
                    {
                        let a = time::Duration::from_nanos(16666667);
                        let percent = cumul.checked_div(FPS_TO_AVERAGE);
                        match percent
                        {
                            Some(d) => {
                                let mut p = (d.as_secs_f32() / a.as_secs_f32()) * 100f32;
                                p = (p * 100f32).round() / 100f32;
                                let mut s: String = p.to_string();
                                s.push_str("%");
                                percent_str = primitive::get_string_primitive(-310f32, 215f32, 14f32, 0.60f32, 0.75f32, &s, &w);
                            }
                            None => ()
                        }

                        let fps = 1_000_000u128 / (cumul.as_micros() / FPS_TO_AVERAGE as u128);
                        let mut s: String = fps.to_string();
                        s.push_str(" FPS");
                        fps_str = primitive::get_string_primitive(-310f32, 230f32, 14f32, 0.60f32, 0.75f32, &s, &w);
                    }

                    frames = 0;
                    cumul = Duration::new(0, 0);
                }

                let time_to_sleep = frame_duration.saturating_sub(elapsed_time);
                std::thread::sleep(time_to_sleep);
                in_game_time += time_to_sleep.as_secs_f32() + elapsed_time.as_secs_f32();

                match myst_ship
                {
                    None => {}
                    Some(spawn_time) => {
                        if in_game_time >= spawn_time
                        {
                            builder::build_mystery_ship(stage, &mut poly_line_entities, &mut rng);
                            myst_ship = None; // Invalidate new myst ship.
                        }
                    }
                }

                if player_dead || level_finished
                {
                    animation_kept -= 0.016f32;
                    if animation_kept < 0f32
                    {
                        if game_finished
                        {
                            return GameFinished;
                        } else {
                            break 'ship_live;
                        }
                    }
                }
            } // For each live
        } // End of InGame loop.
    } // End of run()
} // End of impl SDLModel for InGame


impl InGame
{
    /**
     * Update player ship, depending on player' s input, left right, shooting etc...
     */
    fn update_player_ship_state(e: &mut Entity, l: bool, r: bool, ctrl: bool, m: &mut Vec<Entity>, dt: f32, rng: &mut XorShift32)
    {
        // Handle player interaction.
        if l
        {
            e.omega += SHIP_ANGULAR_ACCELERATION * dt;
            if e.omega > SHIP_MAX_ANGULAR_SPEED
            {
                e.omega = SHIP_MAX_ANGULAR_SPEED;
            }
        }
        if r
        {
            e.omega -= SHIP_ANGULAR_ACCELERATION * dt;
            if e.omega < -SHIP_MAX_ANGULAR_SPEED
            {
                e.omega = -SHIP_MAX_ANGULAR_SPEED;
            }
        }

        // No keys pressed, for playability, don't conserve angular speed,
        // but apply a strong braking.
        if !l  && !r && e.omega.abs() > 1f32
        {
            let mut decel = SHIP_ANGULAR_BRAKING;
            if e.omega > 0f32
            {
                decel *= -1.0;
                e.omega += decel * dt;
                if e.omega < 0f32
                {
                    e.omega = 0f32;
                }
            } else {
                e.omega += decel * dt;
                if e.omega > 0f32
                {
                    e.omega = 0f32;
                }
            }
        }

        if ctrl
        {
            e.reload_delay -= dt;
            if e.reload_delay < 0f32
            {
                m.push(physics::build_missile(e, rng));
                e.reload_delay += MISSILE_RELOAD_DELAY;
            }
        }
    }


    /**
     * This method generates primitives for drawing thumb ship, as remaining lives. Draw live -1
     * ship, current played ship is not counted in remaining.
     */
    fn update_live(lives: u32, w: &Matrix2D<f32>, points: &mut Vec<Point>, index: &mut Vec<usize>)
    {
        points.clear();
        index.clear();

        if lives <= 1 { return; }; // No remaining live, move out !

        let mut entities: Vec<Entity> = Vec::with_capacity(8);
        let mut x: f32 = -300f32;
        let y: f32 = -215f32;

        for _ in 0..lives - 1 {
            let mut e: Entity = Entity::new(Ship, x, y);
            e.scale_x = 0.5f32;
            e.scale_y = 0.6f32;
            entities.push(e);
            x += 15f32;
        }

        primitive::to_poly_lines_primitive(&entities, w, points, index);
    }


    /**
     * Perform test of player ship against ALL asteroids and Mystery ship.
     */
    fn test_player_collision(player: &Entity, ast: &Vec<Entity>) -> bool
    {
        for e in ast {
            let d: Vector2D<f32> = player.pos - e.pos;
            if d.norm() < (player.bounding_circle_radius + e.bounding_circle_radius)
            {
                return true;
            }
        }
        return false;
    }


    /**
     * Perform collision test of a SINGLE missile against ALL asteroids.
     */
    fn test_missiles_asteroid_collision(missile: &Entity, ast: &Vec<Entity>) -> Option<usize>
    {
        for (i, a) in ast.iter().enumerate()
        {
            let d: Vector2D<f32> = missile.pos - a.pos;
            if d.norm() < (a.bounding_circle_radius)
            {
                return Some(i);
            }
        }
        None
    }


    /**
     * Compute once in a level if the mystery ship will come or not. If yes, determine at which time.
     */
    fn rand_mystery_ship(rng: &mut XorShift32) -> Option<f32>
    {
        let c = rng.rand_range(0, (MYSTERY_SHIP_ONE_AGAINST - 1) as i32);
        if c != 0
        {
            None
        } else {
            let t: f32 = rng.rand_float().abs() * MYSTERY_SHIP_MAX_DELAY;
            Some(t)
        }
    }
}

