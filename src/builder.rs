/**
 * This module contains high level code to create, complex multi entities animation or object
 * like explosions, new asteroids.
 */

pub mod builder
{
    use crate::atlas::Shape;
    use crate::atlas::Shape::{BigAsteroid, Mystship, SmallAsteroid};
    use crate::constants::{BIG_ASTEROID_MAX_OMEGA, BIG_ASTEROID_MAX_SPEED, EXPLOSION_MAX_SPEED, SMALL_ASTEROID_MAX_OMEGA, SMALL_ASTEROID_MAX_SPEED, EXPLOSION_MIN_SPEED, SPEED_MIN_COMPONENT, MYST_SHIP_BOUNDING_CIRCLE_RADIUS, MYST_SHIP_CELERITY, MYSTERY_SHIP_SCORE_VALUE, OFF_SCREEN_X, OFF_SCREEN_Y};
    use crate::entity::Entity;
    use crate::Vector2D;
    use crate::rng::rng::XorShift32;


    /**
     * Build all entities corresponding to an explosion.
     *
     * Param x,y : explosion's position.
     * Param par_num : particle number.
     * Param ret : mutable vector used to populate entities.
     * Param rng : rng state for generating PRN.
     */
    pub fn build_explosion(x: f32, y: f32, par_num: u32, ret: &mut Vec<Entity>, rng: &mut XorShift32)
    {
        for _ in 0..par_num {
            let celerity = rng.rand_range(EXPLOSION_MIN_SPEED, EXPLOSION_MAX_SPEED);
            let mut e: Entity = Entity::new(Shape::Point, x, y);
            e.speed = get_random_direction(rng) * celerity as f32;
            ret.push(e);
        }
    }


    /**
     * Generate a random off-screen position, for objet that will enter the game. Entity can´t
     * appear randomly in screen, with risk of collision with player.
     * return : tuple of (x, y) coordinate.
     */
    fn get_random_off_screen_position(rng: &mut XorShift32) -> (f32, f32)
    {
        let x: f32 = rng.rand_range(-OFF_SCREEN_X as i32, OFF_SCREEN_X as i32) as f32;
        let y: f32 = rng.rand_range(-OFF_SCREEN_Y as i32, OFF_SCREEN_Y as i32) as f32;
        let top: i32 = rng.rand_range(0, 1);
        let left: i32 = rng.rand_range(0, 1);

        if top == 1
        {
            // Appear on h border.
            if left == 1
            {
                return (x, OFF_SCREEN_Y);
            } else {
                return (x, -OFF_SCREEN_Y);
            }
        } else // Appear on V border.
        {
            if left == 1
            {
                return (OFF_SCREEN_X, y);
            } else {
                return (-OFF_SCREEN_X, y);
            }
        }
    }


    /**
     * Generate a random vector pointing in any direction. The vector, is normalized.
     */
    #[inline(always)]
    fn get_random_direction(rng: &mut XorShift32) -> Vector2D<f32>
    {
        let xp = rng.rand_float();
        let yp = rng.rand_float();
        let v: Vector2D<f32> = Vector2D::new(xp, yp);
        v.normalize()
    }


    /**
     * Generate a random speed vector. Direction can be any, but we REJECT trajectory too near
     * from those parallel from horizontal or vertical. It's boring to have a ship or an asteroid
     * running on a screen border.
     *
     * So the speed vector is expected to have a minimum value (positive or negative) in each
     * component.
     */
    #[inline(always)]
    fn get_random_speed(rx: f32, ry: f32, rng: &mut XorShift32) -> Vector2D<f32>
    {
        loop {
            let x = rng.rand_range(-rx as i32, rx as i32);
            if x.abs() > SPEED_MIN_COMPONENT {
                loop {
                    let y = rng.rand_range(-ry as i32, ry as i32);
                    if y.abs() > SPEED_MIN_COMPONENT {
                        return Vector2D::new(x as f32, y as f32);
                    }
                }
            }
        }
    }


    /**
     * Build a bunch of asteroids, at random position, with random speed.
     * s : Shape = SmallAsteroid or BigAsteroid.
     * nbr : Number of entity to build.
     * ret : Vector to fill with created entities.
     * rng : reference to the PRNG.
     */
    pub fn build_asteroid(s: Shape, nbr: u32, ret: &mut Vec<Entity>, rng: &mut XorShift32)
    {
        assert!(s == BigAsteroid || s == SmallAsteroid);
        for _ in 0..nbr
        {
            let p = get_random_off_screen_position(rng);
            let mut e = if s == BigAsteroid
            {
                Entity::new_big_asteroid(p.0, p.1)
            } else {
                Entity::new_small_asteroid(p.0, p.1)
            };

            if s == BigAsteroid
            {
                e.speed = get_random_speed(BIG_ASTEROID_MAX_SPEED, BIG_ASTEROID_MAX_SPEED, rng);
                e.omega = rng.rand_range(-BIG_ASTEROID_MAX_OMEGA as i32, BIG_ASTEROID_MAX_OMEGA as i32) as f32;

            } else {
                e.speed = get_random_speed(SMALL_ASTEROID_MAX_SPEED, SMALL_ASTEROID_MAX_SPEED, rng);
                e.omega = rng.rand_range(-SMALL_ASTEROID_MAX_OMEGA as i32, SMALL_ASTEROID_MAX_OMEGA as i32) as f32;
            }

            e.persistent = true;
            ret.push(e);
        }
    }


    /**
     * Build a bunch of asteroids, at random position, with random speed.
     * s : Shape = SmallAsteroid or BigAsteroid.
     * nbr : Number of entity to build.
     * ret : Vector to fill with created entities.
     * rng : reference to the PRNG.
     */
    pub fn build_asteroid_fragment(src: &Entity, nbr: u32, ret: &mut Vec<Entity>, rng: &mut XorShift32)
    {
        for _ in 0..nbr
        {
            let mut e: Entity = Entity::new_small_asteroid(src.pos.x, src.pos.y);
            e.speed = get_random_speed(SMALL_ASTEROID_MAX_SPEED, SMALL_ASTEROID_MAX_SPEED, rng);
            e.omega = rng.rand_range(-SMALL_ASTEROID_MAX_OMEGA as i32, SMALL_ASTEROID_MAX_OMEGA as i32) as f32;
            e.persistent = true;
            ret.push(e);
        }
    }


    /**
     * Build a single mystery ship, at random position, with fixed speed.
     * s : Shape = SmallAsteroid or BigAsteroid.
     * nbr : Number of entity to build.
     * ret : Vector to fill with created entities.
     * rng : reference to the PRNG.
     */
    pub fn build_mystery_ship(stage: u32, ret: &mut Vec<Entity>, rng: &mut XorShift32)
    {
        let p = get_random_off_screen_position(rng);
        let mut e: Entity = Entity::new(Mystship, p.0, p.1);
        e.speed = get_random_speed(MYST_SHIP_CELERITY,MYST_SHIP_CELERITY, rng);
        e.persistent = true;
        e.score_value = MYSTERY_SHIP_SCORE_VALUE * stage;
        e.bounding_circle_radius = MYST_SHIP_BOUNDING_CIRCLE_RADIUS;
        ret.push(e);
    }


    /**
     * Build various Asteroid depending level reached by the player.
     */
    pub fn build_for_stage(stage: u32, ret: &mut Vec<Entity>, rng: &mut XorShift32)
    {
        ret.clear();
        build_asteroid(BigAsteroid, stage, ret, rng);
        build_asteroid(SmallAsteroid, stage, ret, rng);
    }
}