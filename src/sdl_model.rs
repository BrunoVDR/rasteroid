/**
 * SDLModel trait, run method encapsulates SDL main loop, including pumping event, refreshing
 * screen and so on. Attract mode in a game can be a model, while in game  can be another.
 */
use crate::VectorScreen;

#[allow(dead_code)]
pub enum InterModelMsg {
    Exit,
    StartGame,
    GameFinished,
    Error(String),
}

pub trait SDLModel {
    fn run(self: &Self, svs: &mut VectorScreen) -> InterModelMsg;
}
