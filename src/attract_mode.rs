use std::time;
use std::time::{Duration, Instant};

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Point;

use crate::atlas::Shape::{BigAsteroid, SmallAsteroid};
use crate::builder::builder;
use crate::constants::{BEAM_COLOR, CLEAR_COLOR, CONTROLS, CROSS_BEAM_COLOR, FPS_TO_AVERAGE, GAME_OVER, KEYS, RUST_PUB, TITLE};
use crate::entity::Entity;
use crate::physics::physics;
use crate::primitive::primitive;
use crate::sdl_model::{InterModelMsg,SDLModel};
use crate::VectorScreen;
use crate::rng::rng::XorShift32;


pub(crate) struct AttractMode {}


impl SDLModel for AttractMode
{
    fn run(self: &Self, vec_screen: &mut VectorScreen) -> InterModelMsg
    {
        let beam_color : Color = BEAM_COLOR;
        vec_screen.set_cross_beam_color(&CROSS_BEAM_COLOR);

        // Let's get w world matrix to access screen at classic orthogonal system.
        let w = vec_screen.get_world_matrix();
        let mut rng: XorShift32 = XorShift32::new(0xD2B1_77FB); // Should be mutable to alter state !

        // Allocate various entities vectors.
        let mut poly_line_entities: Vec<Entity> = Vec::with_capacity(256);
        let mut poly_line_vec: Vec<Point> = Vec::with_capacity(1024);
        let mut poly_line_offsets: Vec<usize> = Vec::with_capacity(256);

        let mut point_entities: Vec<Entity> = Vec::with_capacity(1024);
        let mut point_vec: Vec<Point> = Vec::with_capacity(1024);


        builder::build_asteroid(BigAsteroid, 12, &mut poly_line_entities, &mut rng);
        builder::build_asteroid(SmallAsteroid, 12, &mut poly_line_entities, &mut rng);
        builder::build_mystery_ship(0, &mut poly_line_entities, &mut rng);


        let title1_segment = primitive::get_centered_string_primitive(150f32, 12f32, 4f32, 4f32, TITLE, &w);
        let title2_segment = primitive::get_centered_string_primitive(110f32, 13f32, 0.5f32, 0.5f32, RUST_PUB, &w);
        let title3_segment = primitive::get_centered_string_primitive(0f32, 13f32, 1f32, 2f32, GAME_OVER, &w);
        let title4_segment = primitive::get_centered_string_primitive(-75f32, 13f32, 1f32, 1f32, KEYS, &w);
        let title5_segment = primitive::get_centered_string_primitive(-125f32, 13f32, 0.75f32, 1f32, CONTROLS, &w);

        let mut fps_str: Vec<Point> = primitive::get_string_primitive(-310f32, 230f32, 14f32, 0.60f32, 0.75f32, "FPS", &w);
        let mut percent_str: Vec<Point> = primitive::get_string_primitive(-310f32, 215f32, 14f32, 0.60f32, 0.75f32, "PERCENT", &w);

        let frame_duration = time::Duration::from_nanos(16666667); // 16ms = 60Hz.
        let mut frames: u32 = 0;
        let mut cumul: Duration = Duration::new(0, 0);
        let mut fps_toggle = false;


        let mut events = vec_screen
            .get_sdl_context()
            .event_pump()
            .expect("Unable to pump SDL events:");

        'main: loop {
            let now = Instant::now();

            for event in events.poll_iter() {
                match event {
                    Event::Quit { .. } => break 'main,
                    Event::KeyDown {
                        keycode: Some(keycode),
                        ..
                    } => {
                        if keycode == Keycode::Escape {
                            break 'main;
                        }

                        if keycode == Keycode::F {
                            fps_toggle = fps_toggle ^ true;
                        }

                        if keycode == Keycode::E {
                            let m = vec_screen.get_enhanced_rendering() ^true;
                            vec_screen.set_enhanced_rendering(m);
                        }

                        if keycode == Keycode::S {
                            return InterModelMsg::StartGame
                        }
                    }

                    _ => {}
                }
            }

            // Clear Vector screen.
            vec_screen.set_beam_color(&beam_color);
            vec_screen.clear(&CLEAR_COLOR);

            physics::move_entities(&mut poly_line_entities, 0.016f32);
            physics::move_entities(&mut point_entities, 0.016f32);

            primitive::to_point_primitive(&point_entities, &w, &mut point_vec);
            primitive::to_poly_lines_primitive(&poly_line_entities, &w, &mut poly_line_vec, &mut poly_line_offsets);

            // Start drawing
            vec_screen.set_beam_color(&beam_color);

            vec_screen.draw_segments(&title1_segment);
            vec_screen.draw_segments(&title2_segment);
            vec_screen.draw_segments(&title3_segment);
            vec_screen.draw_segments(&title4_segment);
            vec_screen.draw_segments(&title5_segment);

            if fps_toggle
            {
                vec_screen.draw_segments(&fps_str);
                vec_screen.draw_segments(&percent_str);
            }

            vec_screen.draw_poly_lines(&poly_line_vec, &poly_line_offsets);
            vec_screen.draw_pixels(&point_vec);

            // Refresh screen.
            vec_screen.present();

            let elapsed_time = now.elapsed();
            cumul = cumul + elapsed_time;
            frames += 1;

            if frames == FPS_TO_AVERAGE
            {
                if fps_toggle
                {

                    let a = time::Duration::from_nanos(16666667);
                    let percent = cumul.checked_div(FPS_TO_AVERAGE);
                    match percent
                    {
                        Some(d) => {
                            let mut p =  (d.as_secs_f32() / a.as_secs_f32()) * 100f32;
                            p = (p *100f32).round() / 100f32;
                            let mut s: String = p.to_string();
                            s.push_str("%");
                            percent_str = primitive::get_string_primitive(-310f32, 215f32, 14f32, 0.60f32, 0.75f32, &s, &w);
                        },
                        None => ()
                    }

                    let fps = 1_000_000u128 / (cumul.as_micros() / FPS_TO_AVERAGE as u128);
                    let mut s: String = fps.to_string();
                    s.push_str(" FPS");
                    fps_str = primitive::get_string_primitive(-310f32, 230f32, 14f32, 0.60f32, 0.75f32, &s, &w);
                }

                frames = 0;
                cumul = Duration::new(0, 0);
            }

            let time_to_sleep = frame_duration.saturating_sub(elapsed_time);
            std::thread::sleep(time_to_sleep);
        }
        InterModelMsg::Exit
    }
}
