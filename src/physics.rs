/**
 * physics.rs : This module contains all physics (mechanics here) related code.
 *
 * Date :  29/09/2022 16h55
 * Author : Bruno Vedder
 */

pub mod physics {
    use crate::constants::{SHIP_THRUST, MISSILE_ACCURACY, MISSILE_VELOCITY, OFF_SCREEN_X, OFF_SCREEN_Y, SHIP_MAX_SPEED, THRUSTER_PARTICLE_ANGLE, THRUSTER_PARTICLE_VELOCITY};
    use crate::entity::Entity;
    use crate::{Matrix2D, Vector2D};
    use crate::rng::rng::XorShift32;


    pub fn move_entities(entities: &mut Vec<Entity>, dt: f32)
    {
        // Done in reverse order in order to keep index consistent despite removal !
        for i in (0..entities.len()).rev() {
            // Compute new position.
            let mut e = &mut entities[i];
            e.pos = e.pos + (e.speed * dt);
            e.theta += e.omega * dt;

            if check_screen_wrap(e)
            {
                entities.swap_remove(i); // O(1) faster than remove(), but order not kept.
            }
        }
    }


    /**
     * Apply realtime newton's mechanic principles on given entities. Compute
     * movement : F=ma, Inertia.
     *
     * param dt : Delta time between last call.
     */
    pub fn move_player_ship_entity(e: &mut Entity, points: &mut Vec<Entity>, dt: f32, rng: &mut XorShift32)
    {
        if e.force_applied
        {
            let part = build_random_reactor_emission(e, rng);
            points.push(part);

            // Player reactor force is always aligned on ship direction.
            let rot = Matrix2D::<f32>::new_rotation(e.theta);
            let mut f = Vector2D::new(0f32, SHIP_THRUST);
            f = rot * f;

            // Compute acceleration :F = ma -> a = F/m.
            let a = f / e.mass;

            // Compute new speed.
            e.speed = e.speed + (a * dt);
            if e.speed.norm() >= SHIP_MAX_SPEED
            {
                e.speed = e.speed.normalize() * SHIP_MAX_SPEED
            }
        }

        // Compute new position.
        e.pos = e.pos + (e.speed * dt);
        e.theta += e.omega * dt;

        check_screen_wrap(e);
    }


    /**
     * Build a single point entity going 180° from given entity theta. Particles
     * are emitted with + or -THRUSTER_PARTICLE_ANGLE° angle from entity speed.
     *
     * @param p
     * @return : new entity built.
     */
    fn build_random_reactor_emission(p: &Entity, rng: &mut XorShift32) -> Entity
    {
        let cone: f32 = rng.rand_range(-THRUSTER_PARTICLE_ANGLE as i32, THRUSTER_PARTICLE_ANGLE as i32) as f32;
        let rot: Matrix2D<f32> = Matrix2D::<f32>::new_rotation(p.theta + 180f32 + cone);
        let mut v: Vector2D<f32> = Vector2D::new(0f32, THRUSTER_PARTICLE_VELOCITY);
        v = rot * v;

        let mut position: Vector2D<f32> = Vector2D::new(0f32, 5f32);
        position = rot * position;

        let mut e: Entity = Entity::new_missile(position.x + p.pos.x, position.y + p.pos.y);
        e.speed = v;
        e
    }


    /**
     * Build a missile entity that starts from ship's nose, in ship's direction
     * with null angular speed.
     *
     * @param p : Entity to launch from (player's ship).
     * @return : Entity.
     */
    pub fn build_missile(p: &Entity, rng: &mut XorShift32) -> Entity
    {
        // + or - x degree accuracy
        let rot: Matrix2D<f32> = Matrix2D::<f32>::new_rotation(p.theta + rng.rand_range(-MISSILE_ACCURACY, MISSILE_ACCURACY) as f32);
        let mut v: Vector2D<f32> = Vector2D::new(0f32, MISSILE_VELOCITY);
        v = rot * v;

        // Compute start position of missile, at ship's nose.
        let mut position: Vector2D<f32> = Vector2D::new(0f32, 7.5f32);
        position = rot * position;

        let mut e: Entity = Entity::new_missile( p.pos.x, p.pos.y);
        e.pos = e.pos + position;
        e.speed = v; //v.add(p.speed); // Galilean speed composition.
        e
    }


    /**
     * The provided entity reach screen horizontal or vertical limit. If it's tagged as persistent,
     * perform a warp (reappear on opposite screen's side, with the same speed). Asteroid stands
     * on a topological torus !
     * Return value: true if a non persistent object goes off-screen and should be deleted. False
     * if the object (wrapped) shouldn't be destroyed.
     */
    #[inline(always)]
    fn check_screen_wrap(e: &mut Entity) -> bool
    {
        if e.pos.x.abs() > OFF_SCREEN_X
        {
            if e.persistent
            {
                e.pos = Vector2D::new(e.pos.x * -1.0, e.pos.y);
            } else {
                return true;
            }
        } else {
            if e.pos.y.abs() > OFF_SCREEN_Y
            {
                if e.persistent
                {
                    e.pos = Vector2D::new(e.pos.x, e.pos.y * -1.0);
                } else {
                    return true;
                }
            }
        }
        false
    }
}
