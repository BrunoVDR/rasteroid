/**
 * sdl_screen_vector.rs : This code is used to provide drawing primitives for vector graphics
 * be used with SDL.
 *
 * Date :  28/09/2022 14h04
 * Author : Bruno Vedder
 */
use sdl2::{Sdl, VideoSubsystem};
use sdl2::pixels::Color;
use sdl2::rect::Point;
use sdl2::render::WindowCanvas;

use crate::Matrix2D;


/**
 * This file contains code related to SDL canvas rendering.
 */
#[allow(dead_code)] // To avoid warning about unused video_subsystem
pub struct VectorScreen {
    sdl_context: Sdl,
    video_subsystem: VideoSubsystem,
    canvas: WindowCanvas,
    width: u32,
    height: u32,
    beam_color : Color,
    cross_beam_color : Color,
    enhanced_rendering : bool,
}


impl VectorScreen {
    /**
     * Kind of Constructor that gather SDL related code here.
     */
    pub fn new(title: &str, width: u32, height: u32) -> VectorScreen {
        let sdl_context = sdl2::init().unwrap();
        let video_subsystem = sdl_context.video().unwrap();

        let window = video_subsystem
            .window(title, width, height)
            .position_centered()
            .build()
            .map_err(|e| e.to_string())
            .unwrap();

        let canvas = window
            .into_canvas()
            .accelerated()
            .build()
            .map_err(|e| e.to_string())
            .unwrap();

        VectorScreen {
            sdl_context,
            video_subsystem,
            canvas,
            width,
            height,
            beam_color : Color::RGBA(200,200,200, 255),
            cross_beam_color : Color::RGBA(0,255,0, 255),
            enhanced_rendering : true,
        }
    }

    pub fn get_sdl_context(&self) -> &Sdl
    {
        &self.sdl_context
    }


    /**
     * Returns a matrix to present change SDL referential to 0,0 screen centered, top positive
     * cartesian system. This is done with symetry among x axis, and a translation to the center.
     * Return: World matrix.
     */
    pub fn get_world_matrix(&mut self) -> Matrix2D<f32>
    {
        // Let's create w world matrix in to access screen at classic orthogonal system.
        let sym_x = Matrix2D::<f32>::new_symmetry_x();
        let wt = Matrix2D::<f32>::new_translation(self.width as f32 / 2f32, self.height as f32 / 2f32);
        wt * sym_x
    }


    /**
     * Draw non contiguous shapes, like letters, cross (next line cannot start from last point).
     * This method is a lot slower than draw_poly_lines.
     */
    pub fn draw_segments(&mut self, points: &Vec<Point>) {

        for i in 0..(points.len() / 2) {
            self.canvas.set_draw_color(self.beam_color);
            let p0 = *(&points[i * 2]);
            let p1 = *(&points[i * 2 + 1]);
            self.canvas.draw_line(p0, p1).expect("draw_line exploded !");
            if self.enhanced_rendering
            {
                self.canvas.set_draw_color(self.cross_beam_color);
                self.canvas.draw_point(p0).expect("TODO: panic message");
                self.canvas.draw_point(p1).expect("TODO: panic message");
            }
        }
    }


    /**
     * Draw contiguous shapes (That can be drawn by continuing from the last line point:
     * Ship, Asteroid etc...
     */
    pub fn draw_poly_lines(&mut self, points: &[Point], rind: &Vec<usize>) {
        let mut start: usize = 0;
        self.canvas.set_draw_color(self.beam_color);

        for end in rind {
            self.canvas.draw_lines(&points[start..*end]).expect("draw_lines exploded !");
            start = *end;
        }
        if self.enhanced_rendering
        {
            self.draw_enhanced_pixels(points);
        }
    }


    /**
     * Draw contiguous shapes (That can be drawn by continuing from the last line point:
     * Ship, Asteroid etc...
     */
    pub fn draw_single_poly_lines(&mut self, points: &[Point]) {
        self.canvas.set_draw_color(self.beam_color);
        self.canvas.draw_lines(&points[..]).expect("draw_lines exploded !");
        if self.enhanced_rendering
        {
            self.draw_enhanced_pixels(points);
        }
    }

    pub fn draw_enhanced_pixels(&mut self, points: &[Point]) {
        self.canvas.set_draw_color(self.cross_beam_color);
        self.canvas.draw_points(points).expect("draw_points exploded !");
    }


    /**
     * Draw many pixels.
     */
    #[inline(always)]
    pub fn draw_pixels(&mut self, points: &[Point]) {
        self.canvas.set_draw_color(self.beam_color);
        // Call SDL.
        self.canvas.draw_points(points).expect("draw_points exploded !");
    }


    /**
     * Clear/Fill screen with given color
     */
    #[inline(always)]
    pub fn clear(&mut self, color: &Color)
    {
        self.canvas.set_draw_color(*color);
        self.canvas.clear();
    }


    /**
     * Apply bufferized rendering primitives (Draw screen).
     */
    #[inline(always)]
    pub fn present(&mut self)
    {
        self.canvas.present();
    }


    /**
     * Set drawing color
     */
    #[inline(always)]
    pub fn set_beam_color(&mut self, color: &Color)
    {
        self.beam_color = *color;
    }

    /**
     * Set drawing color
     */
    #[inline(always)]
    pub fn set_cross_beam_color(&mut self, color: &Color)
    {
        self.cross_beam_color = *color;
    }

    /**
     * Set enhanced mode rendering.
     */
    pub fn set_enhanced_rendering(&mut self, em : bool)
    {
        self.enhanced_rendering = em;
    }

    pub fn get_enhanced_rendering(&mut self) -> bool
    {
        self.enhanced_rendering
    }
}

