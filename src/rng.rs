/**
 * This is a very basic/crappy implementation of xorshift32 algorithm in order to generate
 * cheap random number sequence, without adding a crate dependency.
 *
 * Date :  29/09/2022 08h44.
 * Author : Bruno Vedder
 */
pub mod rng
{
    pub struct XorShift32 {
        state: u32,
    }


    impl XorShift32 {
        pub fn new(seed: u32) -> XorShift32 {
            XorShift32 { state: seed }
        }

        /**
         * return a 'random' u32
         */
        pub fn rand(&mut self) -> u32 {
            /* Algorithm "xor" from p. 4 of Marsaglia, "Xorshift RNGs" */
            let mut x: u32 = self.state;
            x ^= x << 13;
            x ^= x >> 17;
            x ^= x << 5;
            self.state = x;
            x
        }

        /**
         * Return rand between a and b.
         */
        pub fn rand_range(&mut self, a: i32, b: i32) -> i32 {
            let m = (b - a + 1) as u32;
            let ret = a + (self.rand() % m) as i32;
            assert!(ret <= b && ret >= a);
            ret
        }

        /**
         * Return f32 between +1/-1.
         */
        pub fn rand_float(&mut self) -> f32 {
            let x: i32 = self.rand() as i32;
            let ret = ((x as f64) / (<u32>::MAX as f64)) as f32;
            assert!(ret <= 1.0f32 && ret >= -1.0f32);
            ret
        }
    }
}
