use sdl2::pixels::Color;


/**
 * All games constants a located here, with NO exceptions.
 *
 * Date: 23/09/2022 9h12.
 * Author: Bruno Vedder
 */

pub const FPS_TO_AVERAGE: u32 = 120;
pub const SCREEN_WIDTH: u32 = 640;
pub const SCREEN_HEIGHT: u32 = 480;
pub const MISSILE_RELOAD_DELAY: f32 = 0.10f32;
pub const MISSILE_ACCURACY: i32 = 1i32; // seconds
pub const SHIP_THRUST: f32 = 100000f32; // Reactor thrust, newtons.
pub const SHIP_ANGULAR_ACCELERATION: f32 = 1500f32; // Degree/s^2
pub const SHIP_ANGULAR_BRAKING: f32 = 0.8f32 * SHIP_ANGULAR_ACCELERATION;
pub const SHIP_MAX_ANGULAR_SPEED: f32 = 300f32; // Degree/s
pub const SHIP_MAX_SPEED: f32 = 350f32; // Degree/s
pub const SHIP_MASS: f32 = 400f32; // Ship weight, kilograms.
pub const PLAYER_LIVES: u32 = 3;
pub const MISSILE_VELOCITY: f32 = 420f32; // m.s-1 (1 pixel = 1 m).
pub const BIG_ASTEROID_MAX_OMEGA: f32 = 240f32;
pub const BIG_ASTEROID_MAX_SPEED: f32 = 90f32;
pub const SMALL_ASTEROID_MAX_OMEGA: f32 = 360f32;
pub const SMALL_ASTEROID_MAX_SPEED: f32 = 120f32;
pub const THRUSTER_PARTICLE_ANGLE: f32 = 6f32; // Degree
pub const THRUSTER_PARTICLE_VELOCITY: f32 = 360f32; // m/s.
pub const SMALL_ASTEROID_EXPLOSION_PARTICLE_NBR: u32 = 75;
pub const BIG_ASTEROID_EXPLOSION_PARTICLE_NBR: u32 = BIG_ASTEROID_FRAGMENT_NBR * SMALL_ASTEROID_EXPLOSION_PARTICLE_NBR;
pub const SMALL_ASTEROID_SCORE_VALUE: u32 = 13;
pub const BIG_ASTEROID_SCORE_VALUE: u32 = 7;
pub const BIG_ASTEROID_FRAGMENT_NBR: u32 = 4;
pub const MYSTERY_SHIP_SCORE_VALUE: u32 = 37; // 37 * stage !
pub const MYSTERY_SHIP_ONE_AGAINST: u32 = 3; // One chance on 3.
pub const MYSTERY_SHIP_MAX_DELAY: f32 = 60f32; // seconds
pub const PLAYER_SHIP_BOUNDING_RADIUS: f32 = 10f32;
pub const SMALL_ASTEROID_BOUNDING_RADIUS: f32 = 10f32;
pub const BIG_ASTEROID_BOUNDING_RADIUS: f32 = 20f32;
pub const SMALL_ASTEROID_MASS: f32 = 2500f32; // 0.25 * BIG_ASTEROID_MASS !
pub const BIG_ASTEROID_MASS: f32 = 10000f32;  // kg.
pub const SHIP_EXPLOSION_PARTICLE_NBR: u32 = 600;
pub const KEEP_ANIMATION: f32 = 3f32; // Seconds
pub const OFF_SCREEN_X: f32 = ((SCREEN_WIDTH as f32 / 2f32) + BIG_ASTEROID_BOUNDING_RADIUS) as f32;
pub const OFF_SCREEN_Y: f32 = ((SCREEN_HEIGHT as f32 / 2f32) + BIG_ASTEROID_BOUNDING_RADIUS) as f32;
pub const EXPLOSION_MAX_SPEED: i32 = 500; // m.s-1
pub const EXPLOSION_MIN_SPEED: i32 = 150; // m.s-1
pub const SPEED_MIN_COMPONENT: i32 = 5; // Careful, can have big perf impact.
pub const MYST_SHIP_CELERITY: f32 = 200f32;
pub const MYST_SHIP_BOUNDING_CIRCLE_RADIUS : f32 = 12f32;
pub const TITLE: &str = "RASTEROID";
pub const GAME_OVER: &str = "GAME OVER";
pub const RUST_PUB: &str = "RUST IMPLEMENTATION OF AN ASTEROID CLONE";
pub const KEYS: &str = "PRESS S TO START A NEW GAME";
pub const CONTROLS: &str = "USE UP LEFT RIGHT ARROWS TO MOVE SHIP AND CTRL TO FIRE MISSILES";
pub const CROSS_BEAM_COLOR: Color = Color::RGB(255, 255, 128);
pub const BEAM_COLOR: Color = Color::RGB(220, 180, 0);
pub const CLEAR_COLOR: Color = Color::RGB(0, 0, 0);